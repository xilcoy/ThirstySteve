package thirstysteve;

import java.util.*;

import thirstysteve.utils.ThirstUtils;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.src.*;

public class PlayerHandler {
	public static final Map<String, PlayerHandler> ALL_PLAYERS = new HashMap<String, PlayerHandler>();
	
	public EntityPlayer player;
	public ThirstStats stats;
	
	public PlayerHandler(EntityPlayer player, ThirstStats stats) {
		this.player = player;
		this.stats = stats;
	}
	
	public static void addPlayer(String username, PlayerHandler player) {
		if(!ALL_PLAYERS.containsKey(username)) {
			ALL_PLAYERS.put(username, player);
		}
	}
	
	public static PlayerHandler getPlayer(String username) {
		return ALL_PLAYERS.get(username);
	}
	
	/**
	 * Gets the ThirstStats.class instance.
	 * @return the ThirstStats.class instance.
	 */
	public ThirstStats getStats() {
		return stats;
	}

	/**
	 * Sets all the data in ThirstStats to their original values.
	 */
	public void setDefaults() {
		stats.level = 20;
		stats.exhaustion = 0f;
		stats.saturation = 5f;
		stats.healhurtTimer = 0;
		stats.drinkTimer = 0;
		stats.getPoison().setPoisonedTo(false);
		stats.getPoison().setPoisonTime(0);
	}
	
	/** 
	 * Sets player stats to the arguments.
	 * @param level
	 * @param saturation
	 */
	public void setStats(int level, float saturation) {
		stats.level = level;
		stats.saturation = saturation;
	}

	/** 
	 * Sets player stats to the arguments.
	 * @param level
	 * @param saturation
	 */
	public void addStats(int level, float saturation) {
		stats.addStats(level, saturation);
	}
	
	/**
	 * Adds exhaustion to the player.
	 * @param f amount of exhaustion to add.
	 */
	public void addExhaustion(float f) {
		getStats().addExhaustion(f);
	}
}
