package thirstysteve.gui;

import org.lwjgl.opengl.GL11;
import thirstysteve.blocks.*;
import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiButtonMerchant;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.src.*;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

public class GuiUC extends GuiContainer {
	private TileEntityUC uc;
	private Minecraft minecraft = FMLClientHandler.instance().getClient();
	private static final ResourceLocation TEXTURE = new ResourceLocation("thirstysteve", "textures/gui/waterCollector.png");

	public GuiUC(InventoryPlayer var1, TileEntityUC var2) {
		super(new ContainerUC(var1, var2));
		uc = var2;
		mc = minecraft;
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int i, int j) {
		this.fontRenderer.drawString("Underground Collector", 33, 6, 0x404040);
		this.fontRenderer.drawString("Item to Fill", 78, 60, 0xBAB7B6);
		this.fontRenderer.drawString("Inventory", 8, (ySize - 96) + 2, 0x404040);
	}
	
	@Override
	protected void actionPerformed(GuiButton par1GuiButton) {
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int i, int j) {
		World world = minecraft.theWorld;
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		mc.getTextureManager().bindTexture(TEXTURE);
		int var5 = (width - xSize) / 2;
		int var6 = (height - ySize) / 2;
		int var7 = uc.getInternalBucketScaled(12);
		int var8 = uc.getRainMeterScaled(24);
		drawTexturedModalRect(var5, var6, 0, 0, xSize, ySize);
		drawTexturedModalRect(var5 + 57, var6 + 36 + 12 - var7, 176, 12 - var7, 14, var7 + 2);
		drawTexturedModalRect(var5 + 79, var6 + 34, 176, 14, var8 + 1, 16);
		
		if(world.getWorldInfo().getWorldTime() < 12000 && uc.isUnderground(world, uc.xCoord, uc.yCoord, uc.zCoord)) {
			drawTexturedModalRect(var5 + 55, var6 + 16, 176, 31, 18, 18);
		}
	}
}
