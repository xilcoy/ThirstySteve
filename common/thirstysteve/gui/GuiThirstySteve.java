package thirstysteve.gui;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.awt.Color;
import java.util.List;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.*;
import net.minecraft.client.multiplayer.NetClientHandler;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.entity.boss.BossStatus;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.util.Direction;
import net.minecraft.util.FoodStats;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;
import net.minecraft.util.StringUtils;
import net.minecraft.world.EnumSkyBlock;
import net.minecraft.world.chunk.Chunk;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import thirstysteve.PlayerHandler;
import thirstysteve.ThirstySteve;
import thirstysteve.ThirstStats;
import thirstysteve.utils.ConfigHelper;
import thirstysteve.utils.ThirstUtils;

import net.minecraftforge.common.ForgeHooks;

@SideOnly(Side.CLIENT)
public class GuiThirstySteve extends GuiIngame
{
	private static final RenderItem itemRenderer = new RenderItem();
	private final Random rand = new Random();
	private final Minecraft mc;

	private static final ResourceLocation TEXTURE = new ResourceLocation("thirstysteve", "textures/gui/thirstBar.png");

	public GuiThirstySteve()
	{
		super(FMLClientHandler.instance().getClient());
		this.mc = FMLClientHandler.instance().getClient();
	}

	/**
	 * Render the ingame overlay with quick icon bar, ...
	 */
	@Override
	protected void func_110327_a(int par1, int par2)
	{
		super.func_110327_a(par1, par2);
		if (mc.thePlayer.ridingEntity == null) {
			drawThirst();
		}
	}

	/**
	 * Begin THIRSTY STEVE RENDERING
	 */

	public int tellingPlayer;

	public void drawThirst() {
		String modTurnedOff = "Thirsty Steve was turned off!";
		String modTurnedOn = "Thirsty Steve was turned on!";
		ScaledResolution var5 = new ScaledResolution(this.mc.gameSettings, this.mc.displayWidth, this.mc.displayHeight);
		FontRenderer var8 = this.mc.fontRenderer;

		if(ThirstySteve.displayMessage != 0) {
			if(ThirstySteve.displayMessage == 2) {
				tellingPlayer++;
				if(tellingPlayer < 60) {
					drawCenteredString(var8, modTurnedOff, var5.getScaledWidth() / 2, var5.getScaledHeight() / 2, 16777215);
				} else {
					tellingPlayer = 0;
					ThirstySteve.displayMessage = 0;
				}
			} else if(ThirstySteve.displayMessage == 1) {
				tellingPlayer++;
				if(tellingPlayer < 60) {
					drawCenteredString(var8, modTurnedOn, var5.getScaledWidth() / 2, var5.getScaledHeight() / 2, 16777215);
				} else {
					tellingPlayer = 0;
					ThirstySteve.displayMessage = 0;
				}
			} else if(ThirstySteve.displayMessage == 3) {
				tellingPlayer++;
				if(tellingPlayer < 60) {
					drawCenteredString(var8, "The server does not allow you to turn the mod off!", var5.getScaledWidth() / 2, var5.getScaledHeight() / 2, 16777215);
				} else {
					tellingPlayer = 0;
					ThirstySteve.displayMessage = 0;
				}
			} else if(ThirstySteve.displayMessage == 5) {
				tellingPlayer++;
				if(tellingPlayer < 60) {
					drawCenteredString(var8, "Can't turn ThirstySteve off in Hardcore Mode!", var5.getScaledWidth() / 2, var5.getScaledHeight() / 2, 16777215);
				} else {
					tellingPlayer = 0;
					ThirstySteve.displayMessage = 0;
				}
			}
		}

		if(ThirstySteve.modOff == false) {
			mc.getTextureManager().bindTexture(TEXTURE);

			ThirstStats thirststats = PlayerHandler.getPlayer(ThirstUtils.getPlayerName()).getStats();
			int thirstLvl = thirststats.level;
			for (int i13 = 0; i13 < 10; i13++) {
				if(ConfigHelper.oldTextures == true) {
					int width = var5.getScaledWidth() / 2 + 91 - i13 * 8 - 9;
					int height = var5.getScaledHeight() - 59;
					int textureXStart = 2;
					int textureYStart = 2;
					int textureYStart1 = 2;
					int textureEndY = 9;
					int textureEndX = 8;

					if (thirststats.saturation <= 0.0F && updateCounter % (thirstLvl * 3 + 1) == 0) {
						height += rand.nextInt(3) - 1;
					}

					if(ConfigHelper.lightBlueColour == true) {
						textureYStart = 12;
					}

					if (ConfigHelper.meterOnLeft == true) {
						width = var5.getScaledWidth() / 2 - 91 + i13 * 8;
					}

					drawTexturedModalRect(width, height, textureXStart, textureYStart1, textureEndX, textureEndY);

					if (thirststats.getPoison().isPoisoned() == false) {
						if (i13 * 2 + 1 < thirstLvl) {
							drawTexturedModalRect(width, height, textureXStart + 9, textureYStart, textureEndX, textureEndY);
						}

						if (i13 * 2 + 1 == thirstLvl) {
							drawTexturedModalRect(width, height, textureXStart + 18, textureYStart, textureEndX, textureEndY);
						}
					} else if (thirststats.getPoison().isPoisoned() == true) {
						if (i13 * 2 + 1 < thirstLvl) {
							drawTexturedModalRect(width, height, textureXStart + 27, textureYStart, textureEndX, textureEndY);
						}

						if (i13 * 2 + 1 == thirstLvl) {
							drawTexturedModalRect(width, height, textureXStart + 36, textureYStart, textureEndX, textureEndY);
						}
					}
				} else {
					int width = var5.getScaledWidth() / 2 + 91 - i13 * 8 - 9;
					int height = var5.getScaledHeight() - 59;
					int textureXStart = 1;
					int textureYStart = 24;
					int textureYStart1 = 24;
					int textureEndY = 9;
					int textureEndX = 7;

					if (thirststats.saturation <= 0.0F && updateCounter % (thirstLvl * 3 + 1) == 0) {
						height += rand.nextInt(3) - 1;
					}

					if (ConfigHelper.meterOnLeft == true) {
						width = var5.getScaledWidth() / 2 - 91 + i13 * 8;
					}

					if(ConfigHelper.lightBlueColour == true) {
						textureYStart = 34;
					}

					drawTexturedModalRect(width, height, textureXStart, textureYStart1, textureEndX, textureEndY);

					if (thirststats.getPoison().isPoisoned() == false) {
						if (i13 * 2 + 1 < thirstLvl) {
							drawTexturedModalRect(width, height, textureXStart + 8, textureYStart, textureEndX, textureEndY);
						}

						if (i13 * 2 + 1 == thirstLvl) {
							drawTexturedModalRect(width, height, textureXStart + 16, textureYStart, textureEndX, textureEndY);
						}
					} else if (thirststats.getPoison().isPoisoned() == true) {
						if (i13 * 2 + 1 < thirstLvl) {
							drawTexturedModalRect(width, height, textureXStart + 24, textureYStart, textureEndX, textureEndY);
						}

						if (i13 * 2 + 1 == thirstLvl) {
							drawTexturedModalRect(width, height, textureXStart + 32, textureYStart, textureEndX, textureEndY);
						}
					}
				} 
			}
		}
	}
}
