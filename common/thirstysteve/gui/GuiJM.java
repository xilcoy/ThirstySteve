package thirstysteve.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.src.*;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;
import thirstysteve.blocks.*;
import cpw.mods.fml.client.FMLClientHandler;

public class GuiJM extends GuiContainer {
	private TileEntityJM jmInventory;
	private static final ResourceLocation TEXTURE = new ResourceLocation("thirstysteve", "textures/gui/drinksBrewer.png");

	public GuiJM(InventoryPlayer inventoryplayer, TileEntityJM juiceMaker) {
		super(new ContainerJM(inventoryplayer, juiceMaker));
		jmInventory = juiceMaker;
		mc = FMLClientHandler.instance().getClient();
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int i, int j) {
		this.fontRenderer.drawString("Drinks Brewer", 54, 10, 0x404040);
		this.fontRenderer.drawString("Glass", 10, 42, 0x9A9796);
		this.fontRenderer.drawString("Fuel", 78, 55, 0x9A9796);
		this.fontRenderer.drawString("Item", 78, 26, 0x9A9796);
		this.fontRenderer.drawString("Inventory", 8, (ySize - 96) + 2, 0x404040);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int i, int j) {
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		mc.getTextureManager().bindTexture(TEXTURE);
		int l = (width - xSize) / 2;
		int t = (height - ySize) / 2;
		drawTexturedModalRect(l, t, 0, 0, xSize, ySize);

		int w = jmInventory.getJuiceProgressScaled(24);
		drawTexturedModalRect(l + 79, t + 34, 176, 14, w, 16);

		if (jmInventory.getTotalFuelTime() > 0) {
			int h = 13 - jmInventory.getFuelProgressScaled(13);
			drawTexturedModalRect(l + 63, t + 47 + 14 - h, 176, 31 + 13 - h, 14, h);
		}
	}
}
