package thirstysteve;

import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.server.FMLServerHandler;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.server.MinecraftServer;
import net.minecraft.src.*;
import thirstysteve.utils.*;

public class CommonProxy {
	public boolean loadedMod = false;

	public void onLoad() {
		TickRegistry.registerTickHandler(new TickHandler(), cpw.mods.fml.relauncher.Side.SERVER);
	}

	public void onTickInGame() {
		String[] usernames = FMLCommonHandler.instance().getMinecraftServerInstance().getAllUsernames();
		for(int i = 0; i < usernames.length; i++) {
			EntityPlayerMP player = FMLCommonHandler.instance().getMinecraftServerInstance().getConfigurationManager().getPlayerForUsername(usernames[i]);
			if(PlayerHandler.getPlayer(player.username) != null) {
				PlayerHandler.getPlayer(usernames[i]).getStats().onTick(player, player);
			}
		}
	}
}
