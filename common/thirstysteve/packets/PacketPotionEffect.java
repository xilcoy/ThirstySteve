package thirstysteve.packets;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.potion.PotionEffect;
import com.google.common.io.ByteArrayDataInput;
import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.relauncher.*;
import thirstysteve.PacketHandler;

public class PacketPotionEffect extends PacketHandler {
	  
	@Override
	public void readServer(int id, ByteArrayDataInput data, Object[] extradata) {
		EntityPlayer player = (EntityPlayer) extradata[0];
		
		int potionid = data.readInt();
		int duration = data.readInt();
		int amplifier = data.readInt();
		
		player.addPotionEffect(new PotionEffect(potionid, duration * 20, amplifier));
	}
	
	@SideOnly(Side.CLIENT)
	public static void sendPacket(int id, int duration, int amplifier) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream(110);
		DataOutputStream dos = new DataOutputStream(bos);
		
		try {
			dos.writeInt(9);
			dos.writeInt(id);
			dos.writeInt(duration);
			dos.writeInt(amplifier);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		Packet250CustomPayload pkt = new Packet250CustomPayload();
		pkt.channel = "ThirstySteve";
		pkt.data = bos.toByteArray();
		pkt.length = bos.size();
		pkt.isChunkDataPacket = false;
		
		PacketDispatcher.sendPacketToServer(pkt);
	}
}
