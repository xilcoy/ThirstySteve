package thirstysteve.packets;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.server.MinecraftServer;
import com.google.common.io.ByteArrayDataInput;
import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.common.network.Player;
import thirstysteve.PacketHandler;
import thirstysteve.PlayerHandler;
import thirstysteve.utils.ThirstUtils;

public class PacketCommand extends PacketHandler {

	@Override
	public void readClient(int id, ByteArrayDataInput data, Object[] extradata) {
		EntityPlayer player = (EntityPlayer) extradata[0];
		String username = player.username;
		String command = data.readUTF();
		
		if(command.equals("reset")) {
			PlayerHandler.getPlayer(username).setDefaults();
		} else if(command.equals("add") || command.equals("set")) {
			int i = data.readInt();
			float f = data.readInt();
			
			if(command.equals("add")) {
				PlayerHandler.getPlayer(username).addStats(i, f);
			}
			
			if(command.equals("set")) {
				PlayerHandler.getPlayer(username).setStats(i, f);
			}
		}
	}
	
	public static void sendCommand(String username, String command, int i, float f) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream(110);
		DataOutputStream dos = new DataOutputStream(bos);
		EntityPlayerMP player = MinecraftServer.getServer().getConfigurationManager().getPlayerForUsername(username);
		
		try {
			dos.writeInt(7);
			dos.writeUTF(command);
			
			if(command.equals("add") || command.equals("set")) {
				dos.writeInt(i);
				dos.writeFloat(f);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		Packet250CustomPayload pkt = new Packet250CustomPayload();
		pkt.channel = "ThirstySteve";
		pkt.data = bos.toByteArray();
		pkt.length = bos.size();
		pkt.isChunkDataPacket = false;
		PacketDispatcher.sendPacketToPlayer(pkt, (Player) player);
	}
}
