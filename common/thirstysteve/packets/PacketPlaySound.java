package thirstysteve.packets;

import java.io.*;

import thirstysteve.ClientProxy;
import thirstysteve.PacketHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.server.MinecraftServer;
import net.minecraft.src.*;

import com.google.common.io.*;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.common.network.Player;

public class PacketPlaySound extends PacketHandler {
	
	public void readClient(int id, ByteArrayDataInput dat, Object[] extradata) {
		EntityPlayer player = (EntityPlayer) extradata[0];
		FMLClientHandler.instance().getClient().sndManager.playSound("random.drink", (float)player.posX, (float)player.posY, (float)player.posZ, 1f, 1f);
	}
	
	public static void sendPlaySound(EntityPlayer player) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream(110);
		DataOutputStream dos = new DataOutputStream(bos);
		
		try {
			dos.writeInt(2);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		Packet250CustomPayload pkt = new Packet250CustomPayload();
		pkt.channel = "ThirstySteve";
		pkt.data = bos.toByteArray();
		pkt.length = bos.size();
		pkt.isChunkDataPacket = false;
		
		PacketDispatcher.sendPacketToAllAround(player.posX, player.posY, player.posZ, 3d, player.dimension, pkt);
	}

	@Override
	public void readServer(int id, ByteArrayDataInput data, Object[] extradata) {}
}
