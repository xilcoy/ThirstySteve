package thirstysteve.packets;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.packet.Packet250CustomPayload;

import com.google.common.io.ByteArrayDataInput;
import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.common.network.Player;
import thirstysteve.utils.*;
import thirstysteve.PacketHandler;

public class PacketConfig extends PacketHandler {
	
	@Override
	public void readClient(int id, ByteArrayDataInput data, Object[] extradata) {
		boolean b1 = data.readBoolean();
		boolean b2 = data.readBoolean();
		int i1 = data.readInt();
		boolean b3 = data.readBoolean();
		
		ConfigHelper.poisonOn = b1;
		ConfigHelper.peacefulOn = b2;
		ConfigHelper.thirstRate = i1;
		ConfigHelper.permitModOff = b3;
	}
	
	public static void sendConfig(EntityPlayer player) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream(100);
		DataOutputStream dos = new DataOutputStream(bos);
		
		try {
			dos.writeInt(8);
			dos.writeBoolean(ConfigHelper.poisonOn);
			dos.writeBoolean(ConfigHelper.peacefulOn);
			dos.writeInt(ConfigHelper.thirstRate);
			dos.writeBoolean(ConfigHelper.permitModOff);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		Packet250CustomPayload pkt = new Packet250CustomPayload();
		pkt.channel = "ThirstySteve";
		pkt.data = bos.toByteArray();
		pkt.length = bos.size();
		pkt.isChunkDataPacket = false;
		
		PacketDispatcher.sendPacketToPlayer(pkt, (Player) player);
	}
}
