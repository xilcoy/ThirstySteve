package thirstysteve.packets;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.util.DamageSource;

import com.google.common.io.ByteArrayDataInput;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.common.network.Player;
import thirstysteve.PacketHandler;

public class PacketHurtPlayer extends PacketHandler {
	
	public void readServer(int id, ByteArrayDataInput dat, Object[] extradata) {
		((EntityPlayer) extradata[0]).attackEntityFrom(DamageSource.starve, 1);
	}
	
	public static void sendPacket() {
		ByteArrayOutputStream bos = new ByteArrayOutputStream(110);
		DataOutputStream dos = new DataOutputStream(bos);
		
		try {
			dos.writeInt(5);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		Packet250CustomPayload pkt = new Packet250CustomPayload();
		pkt.channel = "ThirstySteve";
		pkt.data = bos.toByteArray();
		pkt.length = bos.size();
		pkt.isChunkDataPacket = false;
		PacketDispatcher.sendPacketToServer(pkt);
	}

	@Override
	public void readClient(int id, ByteArrayDataInput data, Object[] extradata) {}
}
