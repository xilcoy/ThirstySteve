package thirstysteve.packets;

import java.io.*;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.NetLoginHandler;
import net.minecraft.network.packet.NetHandler;
import net.minecraft.network.packet.Packet1Login;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.server.MinecraftServer;
import net.minecraft.src.*;

import com.google.common.io.ByteArrayDataInput;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.*;
import cpw.mods.fml.common.network.*;
import cpw.mods.fml.relauncher.Side;
import thirstysteve.*;
import thirstysteve.*;
import thirstysteve.utils.*;

public class PacketHandleSave extends PacketHandler implements IConnectionHandler {
	
	public void readClient(int id, ByteArrayDataInput dat, Object[] extradata) {
		int level = dat.readInt();
		int healTimer = dat.readInt();
		int drinkTimer = dat.readInt();
		float saturation = dat.readFloat();
		float exhaustion = dat.readFloat();
		String playerName = dat.readLine();
		
		PlayerHandler.addPlayer(playerName, new PlayerHandler((EntityPlayer) extradata[0], new ThirstStats()));
		
		if(ClientProxy.getPlayer().username.equals(playerName)) {
			PlayerHandler.getPlayer(playerName).getStats().level = level;
			PlayerHandler.getPlayer(playerName).getStats().healhurtTimer = healTimer;
			PlayerHandler.getPlayer(playerName).getStats().drinkTimer = drinkTimer;
			PlayerHandler.getPlayer(playerName).getStats().saturation = saturation;
			PlayerHandler.getPlayer(playerName).getStats().exhaustion = exhaustion;
		}
	}
	
	public void readServer(int id, ByteArrayDataInput dat, Object[] extradata) {
		int level = dat.readInt();
		int healTimer = dat.readInt();
		int drinkTimer = dat.readInt();
		float saturation = dat.readFloat();
		float exhaustion = dat.readFloat();
		String playerName = dat.readLine();
		
		writeData(level, healTimer, drinkTimer, saturation, exhaustion, playerName);
		
		ThirstStats stats = PlayerHandler.getPlayer(playerName).getStats();
		stats.level = level;
		stats.healhurtTimer = healTimer;
		stats.drinkTimer = drinkTimer;
		stats.saturation = saturation;
		stats.exhaustion = exhaustion;
	}
	
	/**
	 * Writes all the data from a packet to the nbt.
	 * @param i level
	 * @param j healTimer
	 * @param k drinkTimer
	 * @param f saturation
	 * @param l exhaustion
	 * @param s playerName
	 */
	public void writeData(int i, int j, int k, float f, float g, String s) {
		EntityPlayerMP player = FMLCommonHandler.instance().getMinecraftServerInstance().getConfigurationManager().getPlayerForUsername(s);
		NBTTagCompound oldNBT = player.getEntityData();
		NBTTagCompound nbt = oldNBT.getCompoundTag("ThirstySteve");
		if(!oldNBT.hasKey("ThirstySteve")) {
			oldNBT.setCompoundTag("ThirstySteve", nbt);
		}
		nbt.setInteger("level", i);
		nbt.setFloat("exhaustion", g);
		nbt.setFloat("saturation", f);
		nbt.setInteger("healHurtTimer", k);
		nbt.setInteger("drinkTimer", j);
	}

	/**
	 * Reads data from an individual player nbt storage.
	 * @param s player username
	 */
	public void readData(String s) {
		EntityPlayerMP player = FMLCommonHandler.instance().getMinecraftServerInstance().getConfigurationManager().getPlayerForUsername(s);
		NBTTagCompound oldnbt = player.getEntityData();
		NBTTagCompound nbt = oldnbt.getCompoundTag("ThirstySteve");
		if(nbt.hasKey("level")) {
			PlayerHandler.getPlayer(s).getStats().level = nbt.getInteger("level");
			PlayerHandler.getPlayer(s).getStats().exhaustion = nbt.getFloat("exhaustion");
			PlayerHandler.getPlayer(s).getStats().saturation = nbt.getFloat("saturation");
			PlayerHandler.getPlayer(s).getStats().healhurtTimer = nbt.getInteger("healHurtTimer");
			PlayerHandler.getPlayer(s).getStats().drinkTimer = nbt.getInteger("drinkTimer");
		} else PlayerHandler.getPlayer(s).setDefaults();
		sendSaveData(s, PlayerHandler.getPlayer(s).getStats());
	}

	/**
	 * Sends data for a specific player back to the client or server.
	 * @param s username
	 * @param stats instance
	 */
	public static void sendSaveData(String s, ThirstStats stats) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream(110);
		DataOutputStream dos = new DataOutputStream(bos);

		try {
			dos.writeInt(1);
			dos.writeInt(stats.level);
			dos.writeInt(stats.healhurtTimer);
			dos.writeInt(stats.drinkTimer);
			dos.writeFloat(stats.saturation);
			dos.writeFloat(stats.exhaustion);
			dos.writeBytes(s);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Packet250CustomPayload pkt = new Packet250CustomPayload();
		pkt.channel = "ThirstySteve";
		pkt.data = bos.toByteArray();
		pkt.length = bos.size();
		pkt.isChunkDataPacket = false;
		if (FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT) PacketDispatcher.sendPacketToServer(pkt);
		if (FMLCommonHandler.instance().getEffectiveSide() == Side.SERVER) {
			EntityPlayerMP player = FMLCommonHandler.instance().getMinecraftServerInstance().getConfigurationManager().getPlayerForUsername(s);
			PacketDispatcher.sendPacketToPlayer(pkt, (Player) player);
		}
	}
	
	@Override
	public void playerLoggedIn(Player other, NetHandler netHandler, INetworkManager manager) {
		EntityPlayer player = (EntityPlayer)other;
		PlayerHandler.addPlayer(player.username, new PlayerHandler(player, new ThirstStats()));
		if (FMLCommonHandler.instance().getEffectiveSide() == Side.SERVER) {
			readData(player.username);
			PacketConfig.sendConfig(player);
		}
	}
	
	@Override
	public void connectionClosed(INetworkManager manager) {
	}

	@Override
	public String connectionReceived(NetLoginHandler netHandler, INetworkManager manager) {
		return null;
	}

	@Override
	public void connectionOpened(NetHandler netClientHandler, String server, int port, INetworkManager manager) {
		isRemote = true;
		typeOfServer = 2;
	}

	@Override
	public void connectionOpened(NetHandler netClientHandler, MinecraftServer server, INetworkManager manager) {
		typeOfServer = 1;
		isRemote = true; 
	}

	@Override
	public void clientLoggedIn(NetHandler clientHandler, INetworkManager manager, Packet1Login login) {
	}
}
