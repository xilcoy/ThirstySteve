package thirstysteve.packets;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.util.Random;
import com.google.common.io.ByteArrayDataInput;

import cpw.mods.fml.common.network.PacketDispatcher;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.src.*;
import thirstysteve.PacketHandler;
import thirstysteve.PlayerHandler;
import thirstysteve.utils.ThirstUtils;

public class PacketSendStats extends PacketHandler {
	
	public void readServer(int id, ByteArrayDataInput dat, Object[] extradata) {
		int replenish = dat.readInt();
		float saturation = dat.readFloat();
		boolean poison = dat.readBoolean();
		float amountPoison = 0f;
		
		EntityPlayer player = (EntityPlayer) extradata[0];
		PlayerHandler.getPlayer(player.username).getStats().addStats(replenish, saturation);
		
		if(poison == true) {
			amountPoison = dat.readFloat();
			PlayerHandler.getPlayer(player.username).getStats().getPoison().startPoison(new Random(), amountPoison);
		}
	}
	
	public static void sendStatsToAdd(int i, float f, boolean b, float poison) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream(110);
		DataOutputStream dos = new DataOutputStream(bos);
		
		try {
			dos.writeInt(6);
			dos.writeInt(i);
			dos.writeFloat(f);
			dos.writeBoolean(b);
			dos.writeFloat(poison);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		Packet250CustomPayload pkt = new Packet250CustomPayload();
		pkt.channel = "ThirstySteve";
		pkt.data = bos.toByteArray();
		pkt.length = bos.size();
		pkt.isChunkDataPacket = false;
		
		PacketDispatcher.sendPacketToServer(pkt);
	}
}
