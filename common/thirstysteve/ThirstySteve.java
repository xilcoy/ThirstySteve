package thirstysteve;

import java.util.Random;

import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.*;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.Mod.PostInit;
import cpw.mods.fml.common.event.*;
import cpw.mods.fml.common.network.*;
import cpw.mods.fml.common.registry.*;
import net.minecraft.block.Block;
import net.minecraft.command.CommandHandler;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumArmorMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.server.MinecraftServer;
import net.minecraft.src.*;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import thirstysteve.api.*;
import thirstysteve.blocks.*;
import thirstysteve.drink.Drink;
import thirstysteve.items.*;
import thirstysteve.gui.*;
import thirstysteve.packets.*;
import thirstysteve.utils.*;
import uristqwerty.CraftGuide.api.CraftGuideAPIObject;
import net.minecraftforge.common.*;
import net.minecraftforge.event.*;
import net.minecraftforge.event.Event.Result;
import net.minecraftforge.event.entity.living.*;
import net.minecraftforge.event.entity.player.*;
import net.minecraftforge.event.world.*;
import net.minecraftforge.oredict.ShapedOreRecipe;

@Mod(modid = ThirstUtils.ID, name = ThirstUtils.NAME, version = Version.VERSION, dependencies = "required-after:Forge@[9.11.1.916,);after:craftguide")
@NetworkMod(serverSideRequired = false, clientSideRequired = true, packetHandler = PacketHandler.class, channels = { "ThirstySteve" })
public class ThirstySteve implements IGuiHandler {
	public static CreativeTabs thirstTab;

	public static Block waterCollector;
	public static Block juiceMaker;
	public static Block underCollector;
	public static ItemThirst dFilter; 
	public static ItemThirst filter;
	public static ItemThirst ccFilter;
	public static ItemThirst woodGlass;
	public static ItemThirst canteen;
	public static ItemDrinkable canteenWater;
	public static ItemDrinkable canteenFWater;
	public static ItemDrinkable fBucket;
	public static ItemDrink bottle;
	public static ItemDrink cup;
	
	public static int jmFront = 1;
	public static int rcTop = 0;

	@Instance(ThirstUtils.ID)
	public static ThirstySteve INSTANCE;

	@SidedProxy(clientSide = "thirstysteve.ClientProxy", serverSide = "thirstysteve.CommonProxy")
	public static CommonProxy proxy;

	public static boolean modOff = false;
	public static int displayMessage;
	
	//Networks
	public PacketHandleSave savePacket = new PacketHandleSave();
	public PacketPlaySound soundPacket = new PacketPlaySound();
	public PacketHurtPlayer hurtPlayer = new PacketHurtPlayer();
	public PacketSendStats statsPacket = new PacketSendStats();
	public PacketCommand commandPacket = new PacketCommand();
	public PacketConfig configPacket = new PacketConfig();
	public PacketPotionEffect potionPacket = new PacketPotionEffect();

	private static ConfigHelper config;
	private static CraftGuideAPIObject craftGuideObject;
	
	/**
	 * Called once the mod has been loaded.
	 * @param event
	 */
	@EventHandler
	public void loadConfiguration(FMLPreInitializationEvent event) {
		thirstTab = new CreativeTabThirst("thirstTab");

		config = new ConfigHelper();
		config.setupConfig();

		waterCollector = new BlockRC(config.rcId).setUnlocalizedName("waterCollector").setResistance(5F).setHardness(4F).setCreativeTab(thirstTab);
		juiceMaker = new BlockJM(config.jmId).setUnlocalizedName("juiceMaker").setResistance(5F).setHardness(4F).setCreativeTab(thirstTab);
		underCollector = new BlockUC(config.ucId).setUnlocalizedName("underCollector").setResistance(5F).setHardness(4F).setCreativeTab(thirstTab);
		dFilter = (ItemThirst)new ItemThirst(config.dFilterId).setUnlocalizedName("dFilter").setMaxStackSize(1);
		filter = (ItemThirst)new ItemThirst(config.filterId).setUnlocalizedName("filter").setMaxStackSize(1).setMaxDamage(4);
		ccFilter = (ItemThirst)new ItemThirst(config.ccFilter).setUnlocalizedName("ccFilter").setMaxStackSize(64);
		woodGlass = (ItemThirst)new ItemThirst(config.woodGlassId).setUnlocalizedName("woodGlass");
		canteen = (ItemThirst)new ItemThirst(config.canteenId).setUnlocalizedName("canteen").setMaxStackSize(4);
		canteenWater = (ItemDrinkable)new ItemDrinkable(config.canteenWaterId, 3, 1.2f, false).setPoisonChance(0.2f).setReturn(canteen).setUnlocalizedName("canteenWater").setMaxDamage(4).setMaxStackSize(4);
		canteenFWater = (ItemDrinkable)new ItemDrinkable(config.canteenFWaterId, 5, 1.6f, false).setWaterFillItem(new ItemStack(canteenWater)).setReturn(canteen).setUnlocalizedName("canteenFWater").setMaxDamage(4).setMaxStackSize(4);
		fBucket = (ItemDrinkable)new ItemDrinkable(config.fBucketId, 10, 4f, false).setReturn(Item.bucketEmpty).setUnlocalizedName("freshBucket").setMaxStackSize(config.maxStackSize);
		bottle = (ItemDrink)new ItemDrink(config.bottleDrinkId).setReturn(Item.glassBottle).setUnlocalizedName("bottle");
		cup = (ItemDrink)new ItemDrink(config.woodDrinkId).setReturn(woodGlass).setUnlocalizedName("cup");
		
		GameRegistry.registerBlock(waterCollector, "WaterCollector");
		GameRegistry.registerBlock(juiceMaker, "JuiceMaker");
		GameRegistry.registerBlock(underCollector, "UnderCollector");
		GameRegistry.registerTileEntity(TileEntityJM.class, "DrinksBrewer");
		GameRegistry.registerTileEntity(TileEntityRC.class, "WaterCollector");
		GameRegistry.registerTileEntity(TileEntityUC.class, "UnderCollector");
		GameRegistry.registerCraftingHandler(new CraftingHandler());

		woodGlass.setWaterFillItem(new ItemStack(cup, 1, Drink.water.id));
		canteen.setWaterFillItem(new ItemStack(canteenWater));
		canteenWater.setWaterFillItem(new ItemStack(canteenWater));

		LanguageRegistry.addName(woodGlass, "Empty Cup");
		LanguageRegistry.addName(canteen, "Empty Canteen");
		LanguageRegistry.addName(canteenWater, "Canteen of Water");
		LanguageRegistry.addName(canteenFWater, "Canteen of Clean Water");
		LanguageRegistry.addName(fBucket, "Fresh Water Bucket");
		LanguageRegistry.addName(new ItemStack(bottle, 0, Drink.freshWater.id), "Fresh Water");
		LanguageRegistry.addName(new ItemStack(bottle, 0, Drink.milk.id), "Milk Bottle");
		LanguageRegistry.addName(new ItemStack(bottle, 0, Drink.cMilk.id), "Chocolate Milk");
		LanguageRegistry.addName(new ItemStack(bottle, 0, Drink.beefStew.id), "Beef Stew");
		LanguageRegistry.addName(new ItemStack(bottle, 0, Drink.porkStew.id), "Ham Water");
		LanguageRegistry.addName(new ItemStack(bottle, 0, Drink.mush.id), "Bottled Mushroom Stew");
		LanguageRegistry.addName(new ItemStack(bottle, 0, Drink.mushRed.id), "Bottled Red Mushroom Stew");
		LanguageRegistry.addName(new ItemStack(bottle, 0, Drink.chickenBroth.id), "Chicken Broth");
		LanguageRegistry.addName(new ItemStack(bottle, 0, Drink.carrotJuice.id), "Carrot Juice");
		LanguageRegistry.addName(new ItemStack(bottle, 0, Drink.goldenCarrotJuice.id), "Golden Carrot Juice");
		LanguageRegistry.addName(new ItemStack(bottle, 0, Drink.melonade.id), "Melonade");
		LanguageRegistry.addName(new ItemStack(bottle, 0, Drink.glisteringMelonade.id), "Glistering Melonade");
		LanguageRegistry.addName(new ItemStack(bottle, 0, Drink.appleJuice.id), "Apple Juice");
		LanguageRegistry.addName(new ItemStack(bottle, 0, Drink.appleGoldJuice.id), "Golden Apple Juice");
		LanguageRegistry.addName(new ItemStack(bottle, 0, Drink.pumpkinJuice.id), "Pumpkin Juice");
		LanguageRegistry.addName(new ItemStack(cup, 0, Drink.water.id), "Cup of Water");
	  	LanguageRegistry.addName(new ItemStack(cup, 0, Drink.freshWater.id), "Cup of Fresh Water");
	  	LanguageRegistry.addName(new ItemStack(cup, 0, Drink.milk.id), "Cup of Milk");
	  	LanguageRegistry.addName(new ItemStack(cup, 0, Drink.cMilk.id), "Cup of Chocolate Milk");
	  	LanguageRegistry.addName(new ItemStack(cup, 0, Drink.beefStew.id), "Cup of Beef Stew");
	  	LanguageRegistry.addName(new ItemStack(cup, 0, Drink.porkStew.id), "Cup of Ham Water");
	  	LanguageRegistry.addName(new ItemStack(cup, 0, Drink.mush.id), "Cup of Mushroom Stew");
	  	LanguageRegistry.addName(new ItemStack(cup, 0, Drink.mushRed.id), "Cup of Red Mushroom Stew");
	  	LanguageRegistry.addName(new ItemStack(cup, 0, Drink.chickenBroth.id), "Cup of Chicken Broth");
	  	LanguageRegistry.addName(new ItemStack(cup, 0, Drink.carrotJuice.id), "Cup of Carrot Juice");
	 	LanguageRegistry.addName(new ItemStack(cup, 0, Drink.goldenCarrotJuice.id), "Cup of Golden Carrot Juice");
	 	LanguageRegistry.addName(new ItemStack(cup, 0, Drink.melonade.id), "Cup of Melonade");
	 	LanguageRegistry.addName(new ItemStack(cup, 0, Drink.glisteringMelonade.id), "Cup of Glistering Melonade");
	 	LanguageRegistry.addName(new ItemStack(cup, 0, Drink.appleJuice.id), "Cup of Apple Juice");
	 	LanguageRegistry.addName(new ItemStack(cup, 0, Drink.appleGoldJuice.id), "Cup of Golden Apple Juice");
	 	LanguageRegistry.addName(new ItemStack(cup, 0, Drink.pumpkinJuice.id), "Cup of Pumpkin Juice");
	}

	@EventHandler
	public void initialize(FMLInitializationEvent event) {
		MinecraftForge.EVENT_BUS.register(INSTANCE);
		MinecraftForge.EVENT_BUS.register(proxy);
		NetworkRegistry.instance().registerGuiHandler(this, this);
		NetworkRegistry.instance().registerConnectionHandler(savePacket);
		
		// Juicing recipes
		ThirstUtils.addJMRecipe(Item.beefCooked.itemID, Item.glassBottle.itemID, new ItemStack(bottle, 1, Drink.beefStew.id));
		ThirstUtils.addJMRecipe(Item.porkCooked.itemID, Item.glassBottle.itemID, new ItemStack(bottle, 1, Drink.porkStew.id));
		ThirstUtils.addJMRecipe(Block.mushroomBrown.blockID, Item.glassBottle.itemID, new ItemStack(bottle, 1, Drink.mush.id));
		ThirstUtils.addJMRecipe(Block.mushroomRed.blockID, Item.glassBottle.itemID, new ItemStack(bottle, 1, Drink.mushRed.id));
		ThirstUtils.addJMRecipe(Item.chickenCooked.itemID, Item.glassBottle.itemID, new ItemStack(bottle, 1, Drink.chickenBroth.id));
		ThirstUtils.addJMRecipe(Item.carrot.itemID, Item.glassBottle.itemID,new ItemStack(bottle, 1, Drink.carrotJuice.id));
		ThirstUtils.addJMRecipe(Item.goldenCarrot.itemID, Item.glassBottle.itemID, new ItemStack(bottle, 1, Drink.goldenCarrotJuice.id));
		ThirstUtils.addJMRecipe(Item.melon.itemID, Item.glassBottle.itemID, new ItemStack(bottle, 1, Drink.melonade.id));
		ThirstUtils.addJMRecipe(Item.speckledMelon.itemID, Item.glassBottle.itemID, new ItemStack(bottle, 1, Drink.glisteringMelonade.id));
		ThirstUtils.addJMRecipe(Item.appleRed.itemID, Item.glassBottle.itemID, new ItemStack(bottle, 1, Drink.appleJuice.id));
		ThirstUtils.addJMRecipe(Item.appleGold.itemID, Item.glassBottle.itemID, new ItemStack(bottle, 1, Drink.appleGoldJuice.id));
		ThirstUtils.addJMRecipe(Block.pumpkin.blockID, Item.glassBottle.itemID, new ItemStack(bottle, 1, Drink.pumpkinJuice.id));
		ThirstUtils.addJMRecipe(Item.beefCooked.itemID, woodGlass.itemID, new ItemStack(cup, 1, Drink.beefStew.id));
		ThirstUtils.addJMRecipe(Item.porkCooked.itemID, woodGlass.itemID, new ItemStack(cup, 1, Drink.porkStew.id));
		ThirstUtils.addJMRecipe(Block.mushroomBrown.blockID, woodGlass.itemID, new ItemStack(cup, 1, Drink.mush.id));
		ThirstUtils.addJMRecipe(Block.mushroomRed.blockID, woodGlass.itemID, new ItemStack(cup, 1, Drink.mushRed.id));
		ThirstUtils.addJMRecipe(Item.chickenCooked.itemID, woodGlass.itemID, new ItemStack(cup, 1, Drink.chickenBroth.id));
		ThirstUtils.addJMRecipe(Item.carrot.itemID, woodGlass.itemID, new ItemStack(cup, 1, Drink.carrotJuice.id));
		ThirstUtils.addJMRecipe(Item.goldenCarrot.itemID, woodGlass.itemID, new ItemStack(cup, 1, Drink.goldenCarrotJuice.id));
		ThirstUtils.addJMRecipe(Item.melon.itemID, woodGlass.itemID, new ItemStack(cup, 1, Drink.melonade.id));
		ThirstUtils.addJMRecipe(Item.speckledMelon.itemID, woodGlass.itemID, new ItemStack(cup, 1, Drink.glisteringMelonade.id));
		ThirstUtils.addJMRecipe(Item.appleRed.itemID, woodGlass.itemID, new ItemStack(cup, 1, Drink.appleJuice.id));
		ThirstUtils.addJMRecipe(Item.appleGold.itemID, woodGlass.itemID, new ItemStack(cup, 1, Drink.appleGoldJuice.id));
		ThirstUtils.addJMRecipe(Block.pumpkin.blockID, woodGlass.itemID, new ItemStack(cup, 1, Drink.pumpkinJuice.id));

		// Block Recipes
		GameRegistry.addRecipe(new ItemStack(waterCollector, 1), new Object[]
		{ "***", "*#*", "***", Character.valueOf('*'), Block.cobblestone, Character.valueOf('#'), Item.bucketEmpty, });
		GameRegistry.addRecipe(new ItemStack(juiceMaker, 1), new Object[]
		{ "***", "*#*", "***", Character.valueOf('*'), Block.cobblestone, Character.valueOf('#'), Item.glassBottle, });	
		GameRegistry.addRecipe(new ItemStack(underCollector), new Object[] 
		{ "***", "U U", "UUU", Character.valueOf('*'), Item.silk, Character.valueOf('U'), Block.cobblestone });

		// Item Recipes
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(filter), new Object []
		{ "*!*", "!!!", "*!*", Character.valueOf('*'), "stickWood", Character.valueOf('!'), Item.silk }));		
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ccFilter, 4), new Object []
		{ "***", "*!*", "***", Character.valueOf('*'), "stickWood", Character.valueOf('!'), new ItemStack(Item.coal, 0, 1) }));
		GameRegistry.addShapelessRecipe(new ItemStack(filter), new Object[]
		{ Item.silk, Item.silk, Item.silk, dFilter });
        GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(woodGlass, 5), new Object [] {
        				  "* *", "* *", " * ", Character.valueOf('*'), "plankWood"}));
		GameRegistry.addRecipe(new ItemStack(canteen), new Object[]
						{ " I ", "* *", " * ", Character.valueOf('*'), Item.leather, Character.valueOf('I'), Item.ingotIron});

		//Drink Recipes
		GameRegistry.addRecipe(new ItemStack(bottle, 1, Drink.milk.id), new Object[]
				{ " * ", "^^^", Character.valueOf('*'), Item.bucketMilk, Character.valueOf('^'), Item.glassBottle });
		GameRegistry.addRecipe(new ItemStack(cup, 3, Drink.milk.id), new Object[]
				{ " * ", "^^^", Character.valueOf('*'), Item.bucketMilk, Character.valueOf('^'), woodGlass });
        GameRegistry.addShapelessRecipe(new ItemStack(bottle, 1, Drink.cMilk.id), new Object[]
        		{ new ItemStack(bottle, 0, Drink.milk.id), new ItemStack(Item.dyePowder, 0, 3), Item.sugar, });
        GameRegistry.addShapelessRecipe(new ItemStack(cup, 1, Drink.cMilk.id), new Object[]
        		{ new ItemStack(cup, 0, Drink.milk.id), new ItemStack(Item.dyePowder, 0, 3), Item.sugar, });

		GameRegistry.addShapelessRecipe(new ItemStack(bottle, 1, Drink.freshWater.id), new Object[]
						{ ThirstySteve.ccFilter, new ItemStack(Item.potion, 0, 0) });
		GameRegistry.addShapelessRecipe(new ItemStack(cup, 1, Drink.freshWater.id), new Object[]
				{ ThirstySteve.ccFilter, woodGlass });

		FurnaceRecipes.smelting().addSmelting(Item.potion.itemID, 0, new ItemStack(bottle, 1, Drink.freshWater.getId()), 0.3f);
		FurnaceRecipes.smelting().addSmelting(cup.itemID, Drink.water.id, new ItemStack(cup, 1, Drink.freshWater.id), 0.3f);
		GameRegistry.addSmelting(Item.bucketWater.itemID, new ItemStack(fBucket, 1), 0.4f);

		for(int i = 0; i < 6; i++) {
			FurnaceRecipes.smelting().addSmelting(canteenWater.itemID, i, new ItemStack(canteenFWater, 1, i), 0.3f);
			GameRegistry.addShapelessRecipe(new ItemStack(bottle, 1, Drink.freshWater.id), new Object[]
							{ new ItemStack(ThirstySteve.filter, 0, i), new ItemStack(Item.potion, 0, 0) });
			GameRegistry.addShapelessRecipe(new ItemStack(cup, 1, Drink.freshWater.id), new Object[]
					{ new ItemStack(ThirstySteve.filter, 0, i), new ItemStack(cup, 0, Drink.water.id)});
			GameRegistry.addShapelessRecipe(new ItemStack(canteenFWater, 1, i), new Object[]
					{ ThirstySteve.ccFilter, new ItemStack(canteenWater, 0, i), });
			for (int j = 0; j < 6; j++) {
				GameRegistry.addShapelessRecipe(new ItemStack(canteenFWater, 1, j), new Object[]
								{ new ItemStack(ThirstySteve.filter, 0, i), new ItemStack(canteenWater, 0, j)});
			}
		}
		
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		proxy.onLoad();
		craftGuideObject = new CraftGuide();
	}

	@EventHandler
	public void serverStart(FMLServerStartingEvent event) {
		CommandHandler handler = (CommandHandler) MinecraftServer.getServer().getCommandManager();
		handler.registerCommand(new CommandThirst());
	}
	
	/**
	 * Called when 1 game loop is done.
	 * @param minecraft
	 */
	public void onTickInGame() {
		if (modOff == false) {
			proxy.onTickInGame();
		}
	}

	/**
	 * Called when the player right clicks on a living entity.
	 * @param attack
	 */
	@ForgeSubscribe
	public void onAttack(AttackEntityEvent attack) {
		EntityPlayer player = attack.entityPlayer;
		if (player != null) {
			PlayerHandler handler = PlayerHandler.getPlayer(player.username);
			if (handler != null) {
				handler.addExhaustion(0.6f);
			}
		}
	}

	/**
	 * Called when the player is damaged. i.e when loses health.
	 * @param hurt
	 */
	@ForgeSubscribe
	public void onHurt(LivingHurtEvent hurt) {
		if (hurt.entity instanceof EntityPlayer) {
			EntityPlayer player = (EntityPlayer)hurt.entityLiving;
			if (player != null) {
				PlayerHandler handler = PlayerHandler.getPlayer(player.username);
				if (handler != null) {
					handler.addExhaustion(0.6f);
				}
			}
		}
	}
	
	/**
	 * Determines if the player is jumping.
	 * @return
	 */
	public static boolean isJumping(EntityPlayer player) {
		return player.onGround == false;
	}

	/**
	 * Gets the server container.
	 */
	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		TileEntity tile = world.getBlockTileEntity(x, y, z);
		switch (ID) {
			case 90: return new ContainerJM(player.inventory, (TileEntityJM) tile); 
			case 91: return new ContainerRC(player.inventory, (TileEntityRC) tile); 
			case 92: return new ContainerUC(player.inventory, (TileEntityUC) tile);
		}
		return null;
	}

	/**
	 * Gets the client gui.
	 */
	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		TileEntity tile = world.getBlockTileEntity(x, y, z);
		switch (ID) {
			case 90: return new GuiJM(player.inventory, (TileEntityJM) tile);
			case 91: return new GuiRC(player.inventory, (TileEntityRC) tile);
			case 92: return new GuiUC(player.inventory, (TileEntityUC) tile);
		}
		return null;
	}
}
