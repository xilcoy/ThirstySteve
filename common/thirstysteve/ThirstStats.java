package thirstysteve;

import java.lang.reflect.Field;
import java.util.*;

import thirstysteve.ClientProxy;
import thirstysteve.api.*;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.*;
import cpw.mods.fml.relauncher.ReflectionHelper;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.potion.Potion;
import net.minecraft.src.*;
import net.minecraft.util.FoodStats;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import thirstysteve.packets.*;
import thirstysteve.utils.*;

public class ThirstStats {
	public int level;
	public float saturation;
	public float exhaustion;
	public int healhurtTimer;
	public int drinkTimer;
	public int rainTimer;
	
	private Random random = new Random();
	private PoisonController poisonCon = new PoisonController();
	private ExhaustionController exhauster = new ExhaustionController(this);
	
	public ThirstStats() {
		level = 20;
		saturation = 4f;
		exhaustion = 0f;
		healhurtTimer = 0;
		drinkTimer = 0;
	}

	/**
	 * Holds the Thirst Logic. Controls everything related to that Thirst Bar.
	 * @param player EntityPlayer instance.
	 * @param playerMP null if connected to another server.
	 */
	public void onTick(EntityPlayer player, EntityPlayerMP playerMP) {
		int difSet = player.worldObj.difficultySetting;
		
		if (ConfigHelper.peacefulOn) {
			difSet = 1;
		}
		
		if (exhaustion > 4f) {
			exhaustion = 0f;
			if (saturation > 0f) {
				saturation = saturation - 1f;
			} else if (difSet > 0) {
				level = Math.max(level - 1, 0);
			}
		}
		
		if (level == 0) {
			healhurtTimer++;
			if (healhurtTimer > 120) {
				if (player.getHealth() > 10 || difSet >= 3 || player.getHealth() > 1 && difSet >= 2) {
					healhurtTimer = 0;
					if(FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT) {
						PacketHurtPlayer.sendPacket();
						PacketPotionEffect.sendPacket(Potion.confusion.id, 15, 1);
					} 
					MinecraftForge.EVENT_BUS.post(new ThirstAPI.OnPlayerHurt(player));
				}
			}
		} 
		
		if (player.isSneaking() && player.isInWater() && level < 20) {
			drinkTimer++;
			if (drinkTimer > 16) {
				addStats(1, 0.3F);
				PacketPlaySound.sendPlaySound(player);
				if(FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT) {
					player.worldObj.playSoundAtEntity(player, "random.drink", 1.0f, 1.0f);
				}
				if (ConfigHelper.poisonOn == true) {
					if (random.nextFloat() < poisonCon.getBiomePoison(ThirstUtils.getCurrentBiome(player))) {
						getPoison().startPoison();
					}
				}
				MinecraftForge.EVENT_BUS.post(new ThirstAPI.OnPlayerDrinkWater(player));
				drinkTimer = 0;
			}
		}

		if(level < 7) {
			if(FMLCommonHandler.instance().getEffectiveSide() == Side.SERVER) {
				FoodStats foodStats = playerMP.getFoodStats();
				Field field = FoodStats.class.getDeclaredFields()[3];
				field.setAccessible(true);
				try {
					field.set(foodStats, 0);
				} catch (Exception e) {
					e.printStackTrace();
				} 
			}
		}
		poisonCon.onTick(player);
		exhauster.exhaustPlayer(player);
		
		if (level <= 6) {
			player.setSprinting(false);
		}

		drinkFromRain(player);
	}

	/**
	 * Does whether the player is attempting to drink from rain.
	 * @param player
	 */
	public void drinkFromRain(EntityPlayer player) {
		int x = (int)player.posX;
		int y = (int)player.posY;
		int z = (int)player.posZ;
		
		if(player.isSneaking() == true && player.worldObj.canLightningStrikeAt(x, y, z)
				&& !player.isInWater() && !player.isInsideOfMaterial(Material.water)) {
			rainTimer++;
			if(rainTimer > 40) {
				this.addStats(1, 0.2f);
				PacketPlaySound.sendPlaySound(player);
				if(FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT) {
					player.worldObj.playSoundAtEntity(player, "random.drink", 1.0f, 1.0f);
				} 
				rainTimer = 0;
			}
		}
	}

	
	
	/**
	 * Checks whether there is any block above the player position.
	 * @param x position of the player.
	 * @param y position of the player.
	 * @param z position of the player.
	 * @param world World.class instance.
	 * @return false if there is a block above the player.
	 */
	public boolean isPlayerTopEmpty(int x, int y, int z, World world) {
		for(int i = y; i < 256; i++) {
			if(world.getBlockId(x, i, z) != 0) {
				return false;
			}
		}
		return true;
	}
	
	public PoisonController getPoison() {
		return poisonCon;
	}
	
	public ExhaustionController getExhauster() {
		return exhauster;
	}

	/**
	 * Adds exhaustion
	 * @param par1 Amount to be added.
	 */
	public void addExhaustion(float par1) {
		exhaustion = Math.min(exhaustion + par1, 40F);
	}

	/**
	 * Adds stats to the level and saturation.
	 * @param par1 Amount to add to level.
	 * @param par2 Amount to saturation.
	 */
	public void addStats(int par1, float par2) {
		level = Math.min(par1 + level, 20);
		saturation = Math.min(saturation + (float) par1 * par2 * 2.0F, level);
	}
}
