package thirstysteve.drink;

import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionHelper;

public class Drink {
    /** The array of drink types. */
	public static final Drink[] drinkTypes = new Drink[32];
	public static final Drink water = new Drink(0, 3, 1.2f, false).setDrinkName("water").setPoisonChance(0.2f);
	public static final Drink freshWater = new Drink(1, 7, 2f, false).setDrinkName("freshWater");
	public static final Drink milk = new Drink(2, 6, 1.8f, false).setDrinkName("milk").healFood(2, 1.2f);
	public static final Drink cMilk = new Drink(3, 6, 1.8f, false).setDrinkName("chocolateMilk").healFood(4, 1.2f);
	public static final Drink beefStew = new Drink(4, 5, 1.2f, false).setDrinkName("beefStew").healFood(8, 0.8f);
	public static final Drink porkStew = new Drink(5, 5, 1.2f, false).setDrinkName("porkStew").healFood(5, 1.2f);
	public static final Drink mush = new Drink(6, 3, 1f, false).setDrinkName("mush").healFood(3, 1.2f);
	public static final Drink mushRed = new Drink(7, 4, 1.2f, false).setDrinkName("mushRed").healFood(4, 1.2f);
	public static final Drink chickenBroth = new Drink(8, 6, 1.3f, false).setDrinkName("chickenBroth").healFood(6, 0.6f);
	public static final Drink carrotJuice = new Drink(9, 7, 1.3f, false).setDrinkName("carrotJuice").healFood(4, 0.6f);
	public static final Drink goldenCarrotJuice = new Drink(10, 7, 1.3f, false).setDrinkName("goldenCarrotJuice").healFood(6, 1.2f).setHasEffect().setPotionEffect(PotionHelper.goldenCarrotEffect);
	public static final Drink melonade = new Drink(11, 7, 1.5f, false).setDrinkName("melonade").healFood(2, 0.3f);
	public static final Drink glisteringMelonade = new Drink(12, 12, 2.0f, true).setDrinkName("glisteringMelonade").setHasEffect().setPotionEffect(PotionHelper.speckledMelonEffect);
	public static final Drink appleJuice = new Drink(13, 5, 1.2f, false).setDrinkName("appleJuice").healFood(4, 0.3f);
	public static final Drink appleGoldJuice = new Drink(14, 8, 1.8f, true).setDrinkName("appleGoldJuice").healFood(4, 1.2f).setHasEffect().setPotionEffect(Potion.regeneration.id, 5, 1, 1.0F);
	public static final Drink pumpkinJuice = new Drink(15, 4, 1.4f, false).setDrinkName("pumpkinJuice");

	/** The Id of a Drink object. */
	public final int id;

    /** The name of the Drink. */
	private String name = "";

	private int thirstReplenish;
	private float thirstSaturation;
	private int hungerReplenish;
	private float hungerSaturation;
	private String potionEffect;
	private int potionId;
	private int potionDuration;
	private int potionAmplifier;
	private float potionEffectProbability;
	private boolean hasEffect = false;
	private float poisonChance;
	private boolean alwaysDrinkable;
	
	protected Drink(int id, int replenish, float saturation, boolean alwaysDrinkable) {
		this.id = id;
		drinkTypes[id] = this;
		this.setAlwaysDrinkable(alwaysDrinkable);
		this.thirstReplenish = replenish;
		this.thirstSaturation = saturation;
	}

	/**
	 * returns the ID of the drink
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * Set the drink name.
	 */
	public Drink setDrinkName(String par1Str) {
		this.name = par1Str;
		return this;
	}

	/**
	 * returns the name of the drink
	 */
	public String getName() {
		return this.name;
    }

	/**
	 * Sets a potion effect when the drink is drunk.
	 * @param i Potion ID
	 * @param j Duration
	 * @param k Amplifier
	 * @param f Probability
	 * @return
	 */
	public Drink setPotionEffect(int i, int j, int k, float f) {
		setPotionId(i);
		setPotionDuration(j);
		setPotionAmplifier(k);
		setPotionEffectProbability(f);
		return this;
	}
    /**
     * Sets the string representing this item's effect on a potion when used as an ingredient.
     */
    public Drink setPotionEffect(String par1Str)
    {
        this.potionEffect = par1Str;
        return this;
    }

	/**
	 * Returns a string representing what this item does to a potion.
	 */
	public String getPotionEffect()
	{
    	return this.potionEffect;
	}

	public boolean hasEffect() {
		return hasEffect;
	}

	/**
	 * Makes the item shiny like Golden Apple.
	 * @return this
	 */
	public Drink setHasEffect() {
		hasEffect = true;
		return this;
	}

	/**
	 * Allows the drink to heal the food bar.
	 * @param level amount level.
	 * @param saturation amount saturation.
	 * @return
	 */
	public Drink healFood(int replenish, float saturation) {
		hungerReplenish = replenish;
		hungerSaturation = saturation;
		return this;
	}

	public Drink setPoisonChance(float poisonChance) {
		this.poisonChance = poisonChance;
		return this;
	}

	public int getThirstReplenish() {
		return thirstReplenish;
	}

	public float getThirstSaturation() {
		return thirstSaturation;
	}

	public int getHungerReplenish() {
		return hungerReplenish;
	}

	public float getHungerSaturation() {
		return hungerSaturation;
	}

	public Drink setPotionEffectProbability(float potionEffectProbability) {
		this.potionEffectProbability = potionEffectProbability;
		return this;
	}

	public int getPotionAmplifier() {
		return potionAmplifier;
	}

	public void setPotionAmplifier(int potionAmplifier) {
		this.potionAmplifier = potionAmplifier;
	}

	public int getPotionDuration() {
		return potionDuration;
	}

	public void setPotionDuration(int potionDuration) {
		this.potionDuration = potionDuration;
	}

	public int getPotionId() {
		return potionId;
	}

	public void setPotionId(int potionId) {
		this.potionId = potionId;
	}

	public float getPotionEffectProbability() {
		return potionEffectProbability;
	}

	public boolean isAlwaysDrinkable() {
		return alwaysDrinkable;
	}

	public void setAlwaysDrinkable(boolean alwaysDrinkable) {
		this.alwaysDrinkable = alwaysDrinkable;
	}

	public float getPoisonChance() {
		return poisonChance;
	}
}
