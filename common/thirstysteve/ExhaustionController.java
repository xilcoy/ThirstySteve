package thirstysteve;

import thirstysteve.utils.ConfigHelper;
import thirstysteve.utils.ThirstUtils;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.FMLCommonHandler;
import net.minecraftforge.common.BiomeDictionary;
import static net.minecraftforge.common.BiomeDictionary.Type.*;

public class ExhaustionController {
	
	private ThirstStats stats;
	
	public ExhaustionController(ThirstStats stats) {
		this.stats = stats;
	}
	
	public void exhaustPlayer(EntityPlayer player) {
		float exhaustAmplifier = (float) (isNight(player) ? 0.90 : 1);
		
		int multiplier = BiomeDictionary.isBiomeOfType(ThirstUtils.getCurrentBiome(player), DESERT) ? 2 : 1;
		int movement = ThirstUtils.getMovementStat(player);
		float tweak = (float) ConfigHelper.thirstRate / 10;
		if (player.ridingEntity == null) {
			if (player.isInsideOfMaterial(Material.water)) {
				if (movement > 0) {
					PlayerHandler.getPlayer(player.username).addExhaustion(0.020F * (float) movement * 0.003F * tweak * exhaustAmplifier);
				}
			} else if (player.isInWater()) {
				if (movement > 0) {
					PlayerHandler.getPlayer(player.username).addExhaustion(0.020F * (float) movement * 0.003F * tweak * exhaustAmplifier);
				}
			} else if (player.onGround) {
				if (movement > 0) {
					if (player.isSprinting()) {
						PlayerHandler.getPlayer(player.username).addExhaustion(0.100000199F * (float) movement * 0.02F * multiplier * tweak * exhaustAmplifier);
					} else {
						PlayerHandler.getPlayer(player.username).addExhaustion(0.022F * (float) movement * 0.02F * multiplier * tweak * exhaustAmplifier);
					}
				}
			} else if (ThirstySteve.isJumping(player)) {
				if (player.isSprinting()) {
					PlayerHandler.getPlayer(player.username).addExhaustion(0.06F * multiplier * tweak * exhaustAmplifier);
				} else {
					PlayerHandler.getPlayer(player.username).addExhaustion(0.04F * multiplier * tweak * exhaustAmplifier);
					
				}
			}
		}
	}
	
	public static void addExhaustion(EntityPlayer player, float f) {
		PlayerHandler.getPlayer(player.username).addExhaustion(f);
	}
	
	public boolean isNight(EntityPlayer player) {
		return FMLCommonHandler.instance().getEffectiveSide().isClient()
				? FMLClientHandler.instance().getClient().theWorld.isDaytime() == false : player.worldObj.isDaytime() == false;
	}
}
