package thirstysteve.blocks;

import thirstysteve.ThirstySteve;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotFurnace;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.src.*;

public class ContainerUC extends Container {
	private TileEntityUC uc;
	private int lastRainMeter = 0;
	private int lastInternalBucket = 0;

	public ContainerUC(InventoryPlayer ip, TileEntityUC tile) {
		uc = tile;
		addSlotToContainer(new Slot(tile, 0, 56, 53));
		addSlotToContainer(new SlotFurnace(ip.player, tile, 1, 116, 35));

		int var3;
		for (var3 = 0; var3 < 3; ++var3) {
			for (int var4 = 0; var4 < 9; ++var4) {
				addSlotToContainer(new Slot(ip, var4 + var3 * 9 + 9, 8 + var4 * 18, 84 + var3 * 18));
			}
		}

		for (var3 = 0; var3 < 9; ++var3) {
			addSlotToContainer(new Slot(ip, var3, 8 + var3 * 18, 142));
		}
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int slotIndex) {
		ItemStack returnItem = null;
		Slot slot = (Slot) inventorySlots.get(slotIndex);
		if (slot != null && slot.getHasStack()) {
			ItemStack itemstack = slot.getStack();
			returnItem = itemstack.copy();
			if (slotIndex < 2) {
				if (!mergeItemStack(itemstack, 2, 38, false)) {
					return null;
				}
			} else {
				if (itemstack.itemID == Item.glassBottle.itemID
					|| itemstack.itemID == Item.bucketEmpty.itemID
					|| itemstack.itemID == ThirstySteve.woodGlass.itemID
					|| itemstack.itemID == ThirstySteve.canteen.itemID) {
					if (!mergeItemStack(itemstack, 0, 1, false)) {
						return null;
					}
				} else if (slotIndex >= 2 && slotIndex < 29) {
					if (!mergeItemStack(itemstack, 29, 38, false)) {
						return null;
					}
				} else if (slotIndex >= 29 && slotIndex < 38) {
					if (!mergeItemStack(itemstack, 2, 29, false)) {
						return null;
					}
				}
			}

			if (itemstack.stackSize == 0) {
				slot.putStack((ItemStack) null);
			} else {
				slot.onSlotChanged();
			}

			if (itemstack.stackSize == returnItem.stackSize) {
				return null;
			}

			slot.onPickupFromSlot(player, itemstack);
		}
		return returnItem;
	}

	@Override
	public void detectAndSendChanges() {
		super.detectAndSendChanges();

		for (int var1 = 0; var1 < crafters.size(); ++var1) {
			ICrafting var2 = (ICrafting) crafters.get(var1);
			if (lastRainMeter != uc.RainMeter) {
				var2.sendProgressBarUpdate(this, 0, uc.RainMeter);
			}

			if (lastInternalBucket != uc.internalBucket) {
				var2.sendProgressBarUpdate(this, 1, (int) uc.internalBucket);
			}
		}

		lastRainMeter = uc.RainMeter;
		lastInternalBucket = (int) uc.internalBucket;
	}

	@Override
	public void updateProgressBar(int i, int j) {
		super.updateProgressBar(i, j);
		switch (i) {
		case 0:
			uc.RainMeter = j;
			return;
		case 1:
			uc.internalBucket = j;
			return;
		case 2:
			uc.isActive = j == 1;
		default:
		}

	}

	@Override
	public void addCraftingToCrafters(ICrafting par1ICrafting) {
		super.addCraftingToCrafters(par1ICrafting);
		par1ICrafting.sendProgressBarUpdate(this, 0, this.uc.RainMeter);
		par1ICrafting.sendProgressBarUpdate(this, 1, (int) this.uc.internalBucket);
	}

	@Override
	public boolean canInteractWith(EntityPlayer entityplayer) {
		return uc.isUseableByPlayer(entityplayer);
	}
}
