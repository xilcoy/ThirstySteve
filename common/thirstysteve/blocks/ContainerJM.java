package thirstysteve.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotFurnace;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.src.*;

public class ContainerJM extends Container {
	private TileEntityJM juiceMaker;
	private int lastFuelTime = 0;
	private int lastJuiceTime = 0;
	private int lastTotalFuelTime = 0;
	private int lastTotalJuiceTime = 0;

	public ContainerJM(InventoryPlayer inv, TileEntityJM entity) {
		this.juiceMaker = entity;
		this.addSlotToContainer(new Slot(entity, juiceMaker.itemSlot, 58, 24));
		this.addSlotToContainer(new Slot(entity, juiceMaker.containerSlot, 30, 24));
		this.addSlotToContainer(new Slot(entity, juiceMaker.fuelSlot, 44, 47));
		this.addSlotToContainer(new SlotFurnace(inv.player, entity, juiceMaker.returnSlot, 116, 35));

		for (int i = 0; i < 3; ++i) {
			for (int j = 0; j < 9; ++j) {
				this.addSlotToContainer(new Slot(inv, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
			}
		}

		for (int i = 0; i < 9; ++i) {
			this.addSlotToContainer(new Slot(inv, i, 8 + i * 18, 142));
		}
	}

	@Override
	public void detectAndSendChanges() {
		super.detectAndSendChanges();

		for (int var1 = 0; var1 < this.crafters.size(); ++var1) {
			ICrafting var2 = (ICrafting) this.crafters.get(var1);

			if (this.lastJuiceTime != this.juiceMaker.getJuiceTime()) {
				var2.sendProgressBarUpdate(this, 0, this.juiceMaker.getJuiceTime());
			}

			if (this.lastFuelTime != this.juiceMaker.getFuelTime()) {
				var2.sendProgressBarUpdate(this, 1, this.juiceMaker.getFuelTime());
			}
			
			if (this.lastTotalJuiceTime != this.juiceMaker.getTotalJuiceTime()) {
				var2.sendProgressBarUpdate(this, 2, this.juiceMaker.getJuiceTime());
			}

			if (this.lastTotalFuelTime != this.juiceMaker.getTotalFuelTime()) {
				var2.sendProgressBarUpdate(this, 3, this.juiceMaker.getFuelTime());
			}
		}

		this.lastJuiceTime = this.juiceMaker.getJuiceTime();
		this.lastFuelTime = this.juiceMaker.getFuelTime();
		this.lastTotalJuiceTime = this.juiceMaker.getTotalJuiceTime();
		this.lastTotalFuelTime = this.juiceMaker.getTotalFuelTime();
	}

	@Override
    @SideOnly(Side.CLIENT)
	public void updateProgressBar(int par1, int par2) {
		if (par1 == 0) {
			this.juiceMaker.setJuiceTime(par2);
		} else if (par1 == 1) {
			this.juiceMaker.setFuelTime(par2);
		} else if (par1 == 2) {
			this.juiceMaker.setTotalJuiceTime(par2);
		} else if (par1 == 3) {
			this.juiceMaker.setTotalFuelTime(par2);
		}
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int slotIndex) {
		ItemStack returnItem = null;
		Slot slot = (Slot) this.inventorySlots.get(slotIndex);

		if (slot != null && slot.getHasStack()) {
			ItemStack itemstack = slot.getStack();
			returnItem = itemstack.copy();

			if (slotIndex == juiceMaker.returnSlot) {
				if (!this.mergeItemStack(itemstack, 4, 40, true)) {
					return null;
				}

				slot.onSlotChange(itemstack, returnItem);
			} else if (slotIndex == juiceMaker.itemSlot || slotIndex == juiceMaker.containerSlot || slotIndex == juiceMaker.fuelSlot) {
				if (!this.mergeItemStack(itemstack, 4, 40, false)) {
					return null;
				}
			} else {
				if (JMRecipes.juicing().getItemList().contains(itemstack.itemID)) {
					if (!this.mergeItemStack(itemstack, 0, 1, false)) {
						return null;
					}
				} else if (JMRecipes.juicing().getContainerList().contains(itemstack.itemID)) {
					if (!this.mergeItemStack(itemstack, 1, 2, false)) {
						return null;
					}
				} else if (juiceMaker.getFuelItemTime(itemstack) > 0) {
					if (!this.mergeItemStack(itemstack, 2, 4, false)) {
						return null;
					}
				} else if (slotIndex >= 4 && slotIndex < 31) {
					if (!this.mergeItemStack(itemstack, 31, 40, false)) {
						return null;
					}
				} else if (slotIndex >= 31 && slotIndex < 40 && !this.mergeItemStack(itemstack, 4, 31, false)) {
					return null;
				}
			}

			if (itemstack.stackSize == 0) {
				slot.putStack((ItemStack) null);
			} else {
				slot.onSlotChanged();
			}

			if (itemstack.stackSize == returnItem.stackSize) {
				return null;
			}

			slot.onPickupFromSlot(player, itemstack);
		}
		return returnItem;
	}

	@Override
	public void addCraftingToCrafters(ICrafting par1ICrafting) {
		super.addCraftingToCrafters(par1ICrafting);
		par1ICrafting.sendProgressBarUpdate(this, 0, this.juiceMaker.getJuiceTime());
		par1ICrafting.sendProgressBarUpdate(this, 1, this.juiceMaker.getFuelTime());
		par1ICrafting.sendProgressBarUpdate(this, 2, this.juiceMaker.getTotalJuiceTime());
		par1ICrafting.sendProgressBarUpdate(this, 3, this.juiceMaker.getTotalFuelTime());
	}

	@Override
	public boolean canInteractWith(EntityPlayer entityplayer) {
		return this.juiceMaker.isUseableByPlayer(entityplayer);
	}
}
