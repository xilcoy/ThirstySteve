package thirstysteve.blocks;

import thirstysteve.drink.Drink;
import thirstysteve.items.*;
import net.minecraft.item.*;
import net.minecraft.src.*;
import java.util.*;
import thirstysteve.*;
import thirstysteve.utils.ConfigHelper;
import thirstysteve.items.*;

public class RCRecipes {
	private static final RCRecipes solidifyingBase = new RCRecipes();
	private Map solidifyingList;
	private Map fillTime;

	public static final RCRecipes fill() {
		return solidifyingBase;
	}

	public RCRecipes() {
		solidifyingList = new HashMap();
		fillTime = new HashMap();
		addRecipe(Item.glassBottle.itemID, 200, new ItemStack(ThirstySteve.bottle, 1, Drink.freshWater.id));
		addRecipe(ThirstySteve.woodGlass.itemID, 100, new ItemStack(ThirstySteve.cup, 1, Drink.freshWater.id));
		addRecipe(Item.bucketEmpty.itemID, 600, new ItemStack(ThirstySteve.fBucket));
		addRecipe(ThirstySteve.canteen.itemID, 600, new ItemStack(ThirstySteve.canteenFWater));
	}

	public void addRecipe(int i, int time, ItemStack itemstack) {
		solidifyingList.put(Integer.valueOf(i), itemstack);
		fillTime.put(Integer.valueOf(i), Integer.valueOf(time));
	}

	public ItemStack getSolidifyingResult(int i) {
		return (ItemStack) solidifyingList.get(Integer.valueOf(i));
	}

	public Map getSolidifyingList() {
		return solidifyingList;
	}
	
	public int getFillTimeFor(int itemid) {
		if(fillTime.get(itemid) != null) {
			return (Integer) fillTime.get(itemid);
		}
		else return 200;
	}
}
