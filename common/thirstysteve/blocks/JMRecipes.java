package thirstysteve.blocks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import net.minecraft.item.ItemStack;
import net.minecraft.src.*;
import java.util.Map;

public class JMRecipes {
	private static final JMRecipes juicingBase = new JMRecipes();
	private Map juicingList = new HashMap();
	private List items = new ArrayList();
	private List containers = new ArrayList();
	private List products = new ArrayList();

	public static final JMRecipes juicing() {
		return juicingBase;
	}

	private JMRecipes() {
	}

	public void addJuicing(int itemID, int containerID, ItemStack itemstack) {
		juicingList.put(Arrays.asList(itemID, containerID), itemstack);
		if (!items.contains(itemID)) {
			items.add(itemID);
		}
		if (!containers.contains(containerID)) {
			containers.add(containerID);
		}
		if (!products.contains(itemstack.itemID)) {
			products.add(itemstack.itemID);
		}
	}

	public List getItemList() {
		return items;
	}

	public List getContainerList() {
		return containers;
	}

	public List getProductList() {
		return products;
	}
	
	public ItemStack getJuicingResult(ItemStack item, ItemStack container) {
		if (item == null || container == null) {
			return null;
		}
		return (ItemStack) juicingList.get(Arrays.asList(item.itemID, container.itemID));
	}
}
