package thirstysteve.blocks;

import java.util.Random;
import thirstysteve.ThirstySteve;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.src.*;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockJM extends BlockContainer {
	private Random random = new Random();
	private static boolean keepFreezerInventory = false;

	public BlockJM(int par1) {
		super(par1, Material.rock);
	}

	@Override
	public int idDropped(int i, Random random, int j) {
		return ThirstySteve.INSTANCE.juiceMaker.blockID;
	}

	@Override
	public boolean onBlockActivated(World par1World, int x, int y, int z, EntityPlayer entityplayer, int j, float f, float f1, float f2) {
		entityplayer.openGui(ThirstySteve.INSTANCE, 90, par1World, x, y, z);
		return true;
	}

	@Override
	public void onBlockAdded(World world, int i, int j, int k) {
		super.onBlockAdded(world, i, j, k);
		setDefaultDirection(world, i, j, k);
	}

	@Override
	public TileEntity createNewTileEntity(World world) {
		return new TileEntityJM();
	}

	@Override
	public void onBlockPlacedBy(World world, int i, int j, int k, EntityLivingBase entityliving, ItemStack itemStack) {
		int l = MathHelper.floor_double((double) ((entityliving.rotationYaw * 4F) / 360F) + 0.5D) & 3;
		if (l == 0) {
			world.setBlockMetadataWithNotify(i, j, k, 2, 2);
		}
		if (l == 1) {
			world.setBlockMetadataWithNotify(i, j, k, 5, 2);
		}
		if (l == 2) {
			world.setBlockMetadataWithNotify(i, j, k, 3, 2);
		}
		if (l == 3) {
			world.setBlockMetadataWithNotify(i, j, k, 4, 2);
		}
	}

	@Override
	public void breakBlock(World par1World, int par2, int par3, int par4, int par5, int par6) {
		if (!keepFreezerInventory) {
			TileEntityJM tileentityfreezer = (TileEntityJM) par1World.getBlockTileEntity(par2, par3, par4);
			label0: for (int l = 0; l < tileentityfreezer.getSizeInventory(); l++) {
				ItemStack itemstack = tileentityfreezer.getStackInSlot(l);
				if (itemstack == null) {
					continue;
				}
				float f = random.nextFloat() * 0.8F + 0.1F;
				float f1 = random.nextFloat() * 0.8F + 0.1F;
				float f2 = random.nextFloat() * 0.8F + 0.1F;
				do {
					if (itemstack.stackSize <= 0) {
						continue label0;
					}
					int i1 = random.nextInt(21) + 10;
					if (i1 > itemstack.stackSize) {
						i1 = itemstack.stackSize;
					}
					itemstack.stackSize -= i1;
					EntityItem entityitem = new EntityItem(par1World, (float) par2 + f, (float) par3 + f1, (float) par4 + f2, new ItemStack(itemstack.itemID, i1, itemstack.getItemDamage()));
					float f3 = 0.05F;
					entityitem.motionX = (float) random.nextGaussian() * f3;
					entityitem.motionY = (float) random.nextGaussian() * f3 + 0.2F;
					entityitem.motionZ = (float) random.nextGaussian() * f3;
					par1World.spawnEntityInWorld(entityitem);
				} while (true);
			}
		}
	}

	private void setDefaultDirection(World world, int i, int j, int k) {
		if (world.isRemote) {
			return;
		}
		int l = world.getBlockId(i, j, k - 1);
		int i1 = world.getBlockId(i, j, k + 1);
		int j1 = world.getBlockId(i - 1, j, k);
		int k1 = world.getBlockId(i + 1, j, k);
		byte b = 3;
		if (Block.opaqueCubeLookup[l] && !Block.opaqueCubeLookup[i1]) {
			b = 3;
		}
		if (Block.opaqueCubeLookup[i1] && !Block.opaqueCubeLookup[l]) {
			b = 2;
		}
		if (Block.opaqueCubeLookup[j1] && !Block.opaqueCubeLookup[k1]) {
			b = 5;
		}
		if (Block.opaqueCubeLookup[k1] && !Block.opaqueCubeLookup[j1]) {
			b = 4;
		}
		world.setBlockMetadataWithNotify(i, j, k, b, 2);
	}

	@SideOnly(Side.CLIENT)
	private Icon[] icon;

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister iconRegister)
	{
		icon = new Icon[3];
				
	    icon[0] = iconRegister.registerIcon("thirstysteve:machine_side");
	    icon[1] = iconRegister.registerIcon("thirstysteve:juiceMaker_side");
	    icon[2] = iconRegister.registerIcon("thirstysteve:juiceMaker_front");
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public Icon getIcon(int i, int j)
	{
		if (i == 1)
			return icon[0];
		if (i == 0)
			return icon[0];
		if (i == j || (j < 2 && i == 3))
			return icon[2];
		return icon[1];
	}
}
