package thirstysteve.blocks;

import thirstysteve.ThirstySteve;
import thirstysteve.items.*;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.item.ItemTool;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.src.*;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.ForgeDirection;

public class TileEntityJM extends TileEntity implements ISidedInventory {
	public static final int itemSlot = 0;
	public static final int containerSlot = 1;
	public static final int returnSlot = 2;
	public static final int fuelSlot = 3;
	private ItemStack [] stacks;
	private int juiceTime;
	private int totalJuiceTime;
	private int fuelTime;
	private int totalFuelTime;

	public TileEntityJM() {
		stacks = new ItemStack[4];
		juiceTime = 0;
		setTotalJuiceTime(0);
		fuelTime = 0;
		setTotalFuelTime(0);
	}

	public void updateEntity() {
		boolean inventoryChanged = false;
		
		if (fuelTime > 0) {
			--fuelTime;
		}

		if (!worldObj.isRemote) {
			if (juiceTime > 0) {
				--juiceTime;
			}
			if (fuelTime == 0 && canJuice()) {
				fuelTime = getFuelItemTime(stacks[fuelSlot]);
				setTotalFuelTime(fuelTime);
				if (fuelTime > 0 && stacks[fuelSlot] != null) {
					if (stacks[fuelSlot].getItem().hasContainerItem()) {
						stacks[fuelSlot] = new ItemStack(stacks[fuelSlot].getItem().getContainerItem());
					} else {
						stacks[fuelSlot].stackSize--;
					}
					if (stacks[fuelSlot].stackSize == 0) {
						stacks[fuelSlot] = null;
					}
					inventoryChanged = true;
				}
			}
	
			if (!canJuice() && getTotalJuiceTime() > 0) {
				juiceTime = 0;
				setTotalJuiceTime(0);
			}
			if (getTotalJuiceTime() > 0 && juiceTime == 0) {
				juiceItem();
				inventoryChanged = true;
				setTotalJuiceTime(0);
			}
			if (canJuice() && getTotalJuiceTime() == 0) {
				juiceTime = getItemJuiceTime(stacks[containerSlot]);
				setTotalJuiceTime(juiceTime);
			}
	
			if (inventoryChanged) {
				onInventoryChanged();
			}
		}
	}

	@Override
	public void readFromNBT(NBTTagCompound nbttagcompound) {
		super.readFromNBT(nbttagcompound);
		NBTTagList nbttaglist = nbttagcompound.getTagList("Items");
		stacks = new ItemStack[getSizeInventory()];
		for (int i = 0; i < nbttaglist.tagCount(); i++) {
			NBTTagCompound nbttagcompound1 = (NBTTagCompound) nbttaglist.tagAt(i);
			byte byte0 = nbttagcompound1.getByte("SlotJuicer");
			if (byte0 >= 0 && byte0 < stacks.length) {
				stacks[byte0] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
			}
		}

		setJuiceTime(nbttagcompound.getShort("JuiceTime"));
		setJuiceTime(nbttagcompound.getShort("TotalJuiceTime"));
		setFuelTime(nbttagcompound.getShort("FuelTime"));
		setFuelTime(nbttagcompound.getShort("TotalFuelTime"));
	}

	@Override
	public void writeToNBT(NBTTagCompound nbttagcompound) {
		super.writeToNBT(nbttagcompound);
		nbttagcompound.setShort("JuiceTime", (short) juiceTime);
		nbttagcompound.setShort("TotalJuiceTime", (short) getTotalJuiceTime());
		nbttagcompound.setShort("FuelTime", (short) fuelTime);
		nbttagcompound.setShort("TotalFuelTime", (short) getTotalFuelTime());
		NBTTagList nbttaglist = new NBTTagList();
		for (int i = 0; i < stacks.length; i++) {
			if (stacks[i] != null) {
				NBTTagCompound nbttagcompound1 = new NBTTagCompound();
				nbttagcompound1.setByte("SlotJuicer", (byte) i);
				stacks[i].writeToNBT(nbttagcompound1);
				nbttaglist.appendTag(nbttagcompound1);
			}
		}

		nbttagcompound.setTag("Items", nbttaglist);
	}

	@Override
	public int getSizeInventory() {
		return stacks.length;
	}

	@Override
	public ItemStack getStackInSlot(int i) {
		if (i > 3)
			return null;
		return stacks[i];
	}

	@Override
	public ItemStack decrStackSize(int i, int j) {
		if (stacks[i] != null) {
			if (stacks[i].stackSize <= j) {
				ItemStack itemstack = stacks[i];
				stacks[i] = null;
				return itemstack;
			}
			ItemStack itemstack1 = stacks[i].splitStack(j);
			if (stacks[i].stackSize == 0) {
				stacks[i] = null;
			}
			return itemstack1;
		} else {
			return null;
		}
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int i) {
		if (i > 3)
			return null;

		if (this.stacks[i] != null) {
			ItemStack var2 = this.stacks[i];
			this.stacks[i] = null;
			return var2;
		} else {
			return null;
		}
	}

	@Override
	public void setInventorySlotContents(int i, ItemStack itemstack) {
		if (i > 3)
			return;

		stacks[i] = itemstack;
		if (itemstack != null && itemstack.stackSize > getInventoryStackLimit()) {
			itemstack.stackSize = getInventoryStackLimit();
		}
	}

	@Override
	public String getInvName() {
		return "Juice Maker";
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer entityplayer) {
		if (worldObj.getBlockTileEntity(xCoord, yCoord, zCoord) != this) {
			return false;
		} else {
			return entityplayer.getDistanceSq((double) xCoord + 0.5D, (double) yCoord + 0.5D, (double) zCoord + 0.5D) <= 64D;
		}
	}

	@Override
	public void openChest() {
	}

	@Override
	public void closeChest() {
	}

	public static int getItemJuiceTime(ItemStack itemstack) {
		int fail = 0;
		if (itemstack == null) {
			return 0;
		}
		int i = itemstack.getItem().itemID;
		if (i == Item.glassBottle.itemID) {
			return 200;
		} else if (i == ThirstySteve.woodGlass.itemID) {
			return 100;
		} else {
			return fail;
		}
	}

	private boolean canJuice() {
		if (this.stacks[itemSlot] == null || this.stacks[containerSlot] == null) {
			return false;
		} else {
			ItemStack var1 = JMRecipes.juicing().getJuicingResult(this.stacks[itemSlot], this.stacks[containerSlot]);
			if (var1 == null) return false;
			if (this.stacks[returnSlot] == null) return true;
			if (!this.stacks[returnSlot].isItemEqual(var1)) return false;
			int result = stacks[returnSlot].stackSize + var1.stackSize;
			return (result <= getInventoryStackLimit() && result <= var1.getMaxStackSize());
		}
	}

	public void juiceItem() {
		if (this.canJuice()) {
			ItemStack var1 = JMRecipes.juicing().getJuicingResult(stacks[itemSlot], this.stacks[containerSlot]);

			if (this.stacks[2] == null) {
				this.stacks[2] = var1.copy();
			} else if (this.stacks[2].isItemEqual(var1)) {
				++this.stacks[2].stackSize;
			} else {
				return;
			}

			--this.stacks[0].stackSize;
			if (this.stacks[0].stackSize <= 0) {
				this.stacks[0] = null;
			}

			--this.stacks[1].stackSize;
			if (this.stacks[1].stackSize <= 0) {
				this.stacks[1] = null;
			}
		}
	}

	public static int getFuelItemTime(ItemStack itemstack) {
		if (itemstack == null) {
			return 0;
		}
		int i = itemstack.getItem().itemID;
        Item item = itemstack.getItem();

        if (itemstack.getItem() instanceof ItemBlock && Block.blocksList[i] != null)
        {
            Block block = Block.blocksList[i];

            if (block == Block.woodSingleSlab) {
                return 150;
            }

            if (block.blockMaterial == Material.wood) {
                return 300;
            }

            if (block == Block.coalBlock) {
                return 16000;
            }
        }

        if (item instanceof ItemTool && ((ItemTool) item).getToolMaterialName().equals("WOOD")) return 200;
        if (item instanceof ItemSword && ((ItemSword) item).getToolMaterialName().equals("WOOD")) return 200;
        if (item instanceof ItemHoe && ((ItemHoe) item).getMaterialName().equals("WOOD")) return 200;
        if (i == Item.stick.itemID) return 100;
        if (i == Item.coal.itemID) return 1600;
        if (i == Item.bucketLava.itemID) return 20000;
        if (i == Block.sapling.blockID) return 100;
        if (i == Item.blazeRod.itemID) return 2400;
        return GameRegistry.getFuelValue(itemstack);
	}

	public boolean isJuicing() {
		return getJuiceTime() > 0;
	}

	public int getJuiceProgressScaled(int i) {
		if (getTotalJuiceTime() == 0) {
			return 0;
		}
		return ((getTotalJuiceTime() - juiceTime) * i + (i - 1)) / getTotalJuiceTime();
	}

	public int getFuelProgressScaled(int i) {
		if (getTotalFuelTime() == 0) {
			return 0;
		}
		return ((getTotalFuelTime() - fuelTime) * i + (i - 1)) / getTotalFuelTime();
	}

	public static boolean isItemFuel(ItemStack itemStack) {
		return getFuelItemTime(itemStack) > 0;
	}

	@Override
	public int[] getAccessibleSlotsFromSide(int side) {
		return new int [] {itemSlot, fuelSlot, returnSlot, containerSlot};
	}

	@Override
	public boolean canInsertItem(int slotIndex, ItemStack itemstack, int side) {
		if (stacks[slotIndex] != null && itemstack.itemID == stacks[slotIndex].itemID && stacks[slotIndex].stackSize < stacks[slotIndex].getMaxStackSize()) {
			return true;
		} else if (slotIndex == itemSlot) {
			if (stacks[slotIndex] == null) {
				if (JMRecipes.juicing().getItemList().contains(itemstack.itemID)) {
					return true;
				}
			}
		} else if (slotIndex == fuelSlot) {
			if (stacks[slotIndex] == null) {
				if (getFuelItemTime(itemstack) > 0) {
					return true;
				}
			}
		} else if (slotIndex == containerSlot) {
			if (stacks[slotIndex] == null) {
				if (JMRecipes.juicing().getContainerList().contains(itemstack.itemID)) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean canExtractItem(int slotIndex, ItemStack itemstack, int side) {
		if (side != 1 && slotIndex == returnSlot && itemstack.itemID == stacks[slotIndex].itemID) {
			return true;
		}
		if (side == 1 && slotIndex == fuelSlot && itemstack.itemID == Item.bucketEmpty.itemID) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isInvNameLocalized() {
		return false;
	}

	@Override
	public boolean isItemValidForSlot(int slotIndex, ItemStack itemstack) {
		if (slotIndex == itemSlot) {
			return JMRecipes.juicing().getItemList().contains(itemstack.itemID);
		} else if (slotIndex == fuelSlot) {
			return isItemFuel(itemstack);
		} else if (slotIndex == containerSlot) {
			return JMRecipes.juicing().getContainerList().contains(itemstack.itemID);
		}
		return false;
	}

	public int getJuiceTime() {
		return juiceTime;
	}

	public TileEntityJM setJuiceTime(int time) {
		this.juiceTime = time;
		return this;
	}

	public int getFuelTime() {
		return fuelTime;
	}

	public TileEntityJM setFuelTime(int time) {
		this.fuelTime = time;
		return this;
	}

	public int getTotalJuiceTime() {
		return totalJuiceTime;
	}

	public TileEntityJM setTotalJuiceTime(int totalJuiceTime) {
		this.totalJuiceTime = totalJuiceTime;
		return this;
	}

	public int getTotalFuelTime() {
		return totalFuelTime;
	}

	public TileEntityJM setTotalFuelTime(int totalFuelTime) {
		this.totalFuelTime = totalFuelTime;
		return this;
	}
}
