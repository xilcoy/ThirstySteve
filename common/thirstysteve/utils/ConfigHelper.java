/**
 * Configuration Class. Holds all the variables that can be changed via a text file.
 */
package thirstysteve.utils;

import java.io.File;

import cpw.mods.fml.common.FMLCommonHandler;

import thirstysteve.utils.ThirstUtils;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.Property;

public class ConfigHelper {
	private static Configuration config = new Configuration(new File(ThirstUtils.getDir(), "/config/ThirstySteve.cfg"));

	public static boolean poisonOn;
	public static boolean peacefulOn;
	public static boolean meterOnLeft;
	public static boolean oldTextures;
	public static int thirstRate;
	public static int maxStackSize;
	public static boolean lightBlueColour;
	public static boolean permitModOff;

	public static int filterId;
	public static int dFilterId;
	public static int ccFilter;
	public static int woodGlassId;
	public static int canteenId;
	public static int canteenWaterId;
	public static int canteenFWaterId;	
	public static int fBucketId;

	public static int bottleDrinkId;
	public static int woodDrinkId;

	public static int jmId;
	public static int rcId;
	public static int ucId;
	public static void setupConfig() {		
		config.load();

		poisonOn = get("Poisoning On", "General", true).getBoolean(true);
		peacefulOn = get("Peaceful On", "General", false).getBoolean(false);
		meterOnLeft = get("Meter on leftside", "General", false).getBoolean(false);
		oldTextures = get("Old Meter Textures", "General", false).getBoolean(false);
		thirstRate = get("Exhaustion Rate", "General", 10).getInt();
		maxStackSize = get("Max Stack Size", "General", 10).getInt();
		lightBlueColour = get("Medievor Colours", "General", false).getBoolean(false);
		permitModOff = get("Permit Mod Off", "General", true).getBoolean(true);

		filterId = get("Filter", "Item", 800).getInt();
		dFilterId = get("Dirty Filter", "Item", 801).getInt();
		ccFilter = get("Charcoal Filter", "Item", 802).getInt();
		bottleDrinkId = get("Potion Drink", "Item", 803).getInt();
		woodDrinkId = get("Wood Drink", "Item", 804).getInt();
		woodGlassId = get("Wood Glass", "Item", 805).getInt();
		canteenId = get("Canteen", "Item", 806).getInt();
		canteenWaterId = get("Canteen Water", "Item", 807).getInt();
		canteenFWaterId = get("Canteen Filter Water", "Item", 808).getInt();	
		fBucketId = get("Fresh Water Bucket", "Item", 809).getInt();
	
		jmId = get("Drinks Brewer", "Block", 3505).getInt();
		rcId = get("Rain Collector", "Block", 3506).getInt();
		ucId = get("Underground Collector", "Block", 3508).getInt();
		
		config.save();
	}
	
	public static Property get(String key, String category, Object defaultValue) {
		if(defaultValue instanceof Integer) return config.get(category, key, (Integer)defaultValue);
		else if(defaultValue instanceof Boolean) return config.get(category, key, (Boolean)defaultValue);
		return null;
	}
}
