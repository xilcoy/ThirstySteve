package thirstysteve.utils;

public class Version {
	public static final String VERSION = "@VERSION@";
	public static final String BUILD_NUMBER = "@BUILD_NUMBER@";
	
	public static String getVersion() {
		return VERSION + " (:" + BUILD_NUMBER + ")";
	}
}
