package thirstysteve.utils;

import java.util.EnumSet;
import org.lwjgl.input.Keyboard;

import thirstysteve.utils.*;
import thirstysteve.ThirstySteve;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.TickType;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;

public class KeyHandler extends cpw.mods.fml.client.registry.KeyBindingRegistry.KeyHandler {

	public static KeyBinding lKey = new KeyBinding("TM On/Off", Keyboard.KEY_L);
	public boolean doneStuff = false;
	
	public KeyHandler() {
		super(new KeyBinding[] { lKey }, new boolean[] { false });
	}

	@Override
	public String getLabel() {
		return null;
	}

	@Override
	public void keyDown(EnumSet<TickType> types, KeyBinding kb, boolean tickEnd, boolean isRepeat) {
		Minecraft craft = FMLClientHandler.instance().getClient();
		if(FMLClientHandler.instance().getClient().currentScreen == null) {
			if(kb.keyCode == Keyboard.KEY_L) {
				if (doneStuff == false ) {
					if(ConfigHelper.permitModOff == true && FMLClientHandler.instance().getClient().theWorld.getWorldInfo().isHardcoreModeEnabled() == false) {
						ThirstySteve.modOff = !ThirstySteve.modOff;
						if(ThirstySteve.modOff == false) {
							ThirstySteve.displayMessage = 1;
							doneStuff = true;
						} 
						
						if(ThirstySteve.modOff == true) {
							ThirstySteve.displayMessage = 2;
							doneStuff = true;
						}
					} else {
						if(FMLClientHandler.instance().getClient().theWorld.getWorldInfo().isHardcoreModeEnabled() == true) ThirstySteve.displayMessage = 5;
						else ThirstySteve.displayMessage = 3;
					}
				}
			}
		}
	}

	@Override
	public void keyUp(EnumSet<TickType> types, KeyBinding kb, boolean tickEnd) {
		doneStuff = false;
	}

	@Override
	public EnumSet<TickType> ticks() {
		return EnumSet.of(TickType.PLAYER);
	}
}