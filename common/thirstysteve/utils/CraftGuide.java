package thirstysteve.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import thirstysteve.ThirstySteve;
import thirstysteve.blocks.BlockJM;
import thirstysteve.blocks.JMRecipes;
import thirstysteve.blocks.RCRecipes;
import uristqwerty.CraftGuide.api.CraftGuideAPIObject;
import uristqwerty.CraftGuide.api.ExtraSlot;
import uristqwerty.CraftGuide.api.ItemSlot;
import uristqwerty.CraftGuide.api.RecipeGenerator;
import uristqwerty.CraftGuide.api.RecipeProvider;
import uristqwerty.CraftGuide.api.RecipeTemplate;
import uristqwerty.CraftGuide.api.Slot;
import uristqwerty.CraftGuide.api.SlotType;

public class CraftGuide extends CraftGuideAPIObject implements RecipeProvider {
	
	@Override
	public void generateRecipes(RecipeGenerator generator) {
		addJuiceMakerRecipes(generator);
		addRainCollectorRecipes(generator);
	}

	private void addJuiceMakerRecipes(RecipeGenerator generator) {
		Block jm = ThirstySteve.juiceMaker;
		ItemStack machine = new ItemStack(jm);
		Slot[] recipeSlots = new Slot[4];
		List itemList = JMRecipes.juicing().getItemList();
		List containerList = JMRecipes.juicing().getContainerList();
	
		recipeSlots[0] = new ItemSlot(12, 21, 16, 16, false).drawOwnBackground();
		recipeSlots[1] = new ItemSlot(12, 39, 16, 16, false).drawOwnBackground();
		recipeSlots[2] = new ItemSlot(48, 30, 16, 16, true).setSlotType(SlotType.OUTPUT_SLOT).drawOwnBackground();
		recipeSlots[3] = new ExtraSlot(30, 30, 16, 16, machine).clickable().showName().setSlotType(SlotType.MACHINE_SLOT);

		RecipeTemplate template = generator.createRecipeTemplate(recipeSlots, machine);
		for (int j = 0; j < JMRecipes.juicing().getContainerList().size(); ++j) {
			ItemStack container = new ItemStack(Item.itemsList[(Integer) containerList.get(j)]);

			for (int i = 0; i < JMRecipes.juicing().getItemList().size(); ++i) {
				ItemStack input = new ItemStack(Item.itemsList[(Integer) itemList.get(i)]);

				ItemStack result = JMRecipes.juicing().getJuicingResult(input, container);

				if (result != null) {
					generator.addRecipe(template, new Object[] {
							input,
							container,
							result,
							machine
					});
				}
			}
		}
	}
	
	private void addRainCollectorRecipes(RecipeGenerator generator) {
		ItemStack type = new ItemStack(ThirstySteve.waterCollector);
		ArrayList<Object> machine = new ArrayList<Object>();
		machine.add(type);
		machine.add(new ItemStack(ThirstySteve.underCollector));
		Slot[] recipeSlots = new Slot[3];	
		recipeSlots[0] = new ItemSlot(12, 30, 16, 16, false).drawOwnBackground();
		recipeSlots[1] = new ItemSlot(48, 30, 16, 16, true).setSlotType(SlotType.OUTPUT_SLOT).drawOwnBackground();
		recipeSlots[2] = new ExtraSlot(30, 30, 16, 16, machine).clickable().showName().setSlotType(SlotType.MACHINE_SLOT);

		RecipeTemplate template = generator.createRecipeTemplate(recipeSlots, type);
		Set recipeSet = RCRecipes.fill().getSolidifyingList().entrySet();
		Iterator it = recipeSet.iterator();
		while (it.hasNext()) {
			Map.Entry pairs = (Map.Entry)it.next();
			ItemStack container = new ItemStack(Item.itemsList[(Integer)pairs.getKey()]);

			ItemStack result = (ItemStack)pairs.getValue();

			if (result != null) {
				generator.addRecipe(template, new Object[] {
						container,
						result,
						machine
				});
			}
		}
	}
}
