package thirstysteve.utils;

import java.io.*;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

import thirstysteve.*;
import thirstysteve.blocks.*;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.*;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.src.*;
import net.minecraft.util.MathHelper;
import net.minecraft.world.biome.BiomeGenBase;

public class ThirstUtils {
	public static final String NAME = "Thirsty Steve";
	public static final String ID = "ThirstySteve";
	
	public static String getPlayerName() {
		return ClientProxy.getPlayer().username;
	}
	
	/**
	 * Gets the players current speed in blocks per tick.
	 * @param entityplayer instance.
	 * @return players current speed.
	 */
	public static int getMovementStat(EntityPlayer entityplayer) {
		double s = entityplayer.posX;
		double s1 = entityplayer.posY;
		double s2 = entityplayer.posZ;
		double d = s - entityplayer.prevPosX;
		double d1 = s1 - entityplayer.prevPosY;
		double d2 = s2 - entityplayer.prevPosZ;
		return Math.round(MathHelper.sqrt_double(d * d + d1 * d1 + d2 * d2) * 100F);
	}

	/**
	 * Gets the current biome the player is in.
	 * @param entityplayer
	 * @return the current biome of the player.
	 */
	public static BiomeGenBase getCurrentBiome(EntityPlayer entityplayer) {
		return entityplayer.worldObj.getWorldChunkManager().getBiomeGenAt((int) entityplayer.posX, (int) entityplayer.posZ);
	}

	public static void setModUnloaded() {
		ThirstySteve.proxy.loadedMod = false;
		PacketHandler.isRemote = false;
	}

	@SideOnly(Side.CLIENT)
	public static boolean isClientHost() {
		if(FMLCommonHandler.instance().getSide() == Side.CLIENT) {
			String name = ClientProxy.getPlayer().username;
			if(PacketHandler.isRemote && PacketHandler.typeOfServer == 1) {
				MinecraftServer server = FMLClientHandler.instance().getServer();
				if(server.getServerOwner().equals(name)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Adds a recipe to the Drinks Brewer.
	 * @param id Item that is placed in the top Drinks Brewer Slot.
	 * @param item The Item that is returned after the item (int id) is brewed.
	 */
	public static void addJMRecipe(int id, int containerID, ItemStack item) {
		JMRecipes.juicing().addJuicing(id, containerID, item);
	}
	
	/**
	 * Adds a recipe to the Rain Collector
	 * @param id The id of the item to fill.
	 * @param timeToFill Amount of time taken to fill the item. For reference Glass Bottle = 200, Bucket = 600;
	 * @param return1 The filled item.
	 */
	public static void addRCRecipe(int id, int timeToFill, ItemStack return1) {
		RCRecipes.fill().addRecipe(id, timeToFill, return1);
	}

	public static String getDir() {
		File s = ObfuscationReflectionHelper.getPrivateValue(Loader.class, Loader.instance(), "minecraftDir");
		return s.getAbsolutePath();
	}

	/**
	 * Prints to either the console or the minecraft server window depending on
	 * which side we are current at.
	 * @param obj something to print.
	 */
	public static void print(Object obj) {
		if (FMLCommonHandler.instance().getSide() == Side.CLIENT) {
			System.out.println("[ThirstySteve] " + obj);
		} else {
			FMLCommonHandler.instance().getMinecraftServerInstance().logInfo("[ThirstySteve] "+ obj.toString());
		}
	}
	
	public static void printValues(String s, Object...objects) {
		print(String.format(s, objects));
	}

	public static void addURLToSystemClassLoader(URL url) throws Exception {
		URLClassLoader systemClassLoader = (URLClassLoader) ClassLoader.getSystemClassLoader();
		Class<URLClassLoader> classLoaderClass = URLClassLoader.class;
		Method method = classLoaderClass.getDeclaredMethod("addURL", new Class[] { URL.class });
		method.setAccessible(true);
		method.invoke(systemClassLoader, new Object[] { url });
	}
}