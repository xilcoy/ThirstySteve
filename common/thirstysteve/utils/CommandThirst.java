package thirstysteve.utils;

import thirstysteve.PlayerHandler;
import thirstysteve.packets.PacketCommand;
import thirstysteve.utils.ThirstUtils;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatMessageComponent;

public class CommandThirst extends CommandBase {

	@Override
	public String getCommandName() {
		return "thirst";
	}

	@Override
	public void processCommand(ICommandSender sender, String[] arg) {
		if(arg.length <= 0 || arg[0].equals("help") || arg[0].equals("?") ) {
			sender.sendChatToPlayer(ChatMessageComponent.createFromText("The following commands are available: \n" + "/thirst reset [username] 'Resets the players current statistics.'"));
			sender.sendChatToPlayer(ChatMessageComponent.createFromText("/thirst set <bar> <saturation> [username] 'Sets the players stats to the given values.'"));
			sender.sendChatToPlayer(ChatMessageComponent.createFromText("/thirst add <bar replenish> <saturation> [username] 'Adds the stats to that player'"));	
		} else if(arg[0].equals("reset")) {
			String username = "";
			if(arg.length > 1)
				username = arg[1];
			else
				username = sender.getCommandSenderName();

			for(int i = 0; i < MinecraftServer.getServer().getAllUsernames().length; i++) {
				if(MinecraftServer.getServer().getAllUsernames()[i].equalsIgnoreCase(username)) {
					String actualName = MinecraftServer.getServer().getAllUsernames()[i];
					PlayerHandler.getPlayer(actualName).setDefaults();
					notifyAdmins(sender, String.format("%s's stats were reset", username), new Object [] {});
					PacketCommand.sendCommand(actualName, "reset", 0, 0f);
					return;
				}
			}
			
			sender.sendChatToPlayer(ChatMessageComponent.createFromText("The player '" + username + "' can not be found!"));
		} else if(arg[0].equals("set") || arg[0].equals("add")) {
			String username;	

			if(arg.length < 3) {
				sender.sendChatToPlayer(ChatMessageComponent.createFromText("Invalid command usage. To set the stats to something use: \n/thirst set <replenish> <saturation> [username]"));
				return;
			} else if (arg.length > 3)
				username = arg[3];
			else
				username = sender.getCommandSenderName();

			int rep = Integer.parseInt(arg[1]);
			float sat = Float.parseFloat(arg[2]);
			
			for(int i = 0; i < MinecraftServer.getServer().getAllUsernames().length; i++) {
				if(MinecraftServer.getServer().getAllUsernames()[i].equalsIgnoreCase(username)) {
					String actualName = MinecraftServer.getServer().getAllUsernames()[i];
					if (arg[0].equals("set"))
						PlayerHandler.getPlayer(actualName).setStats(rep, sat);
					else
						PlayerHandler.getPlayer(actualName).addStats(rep, sat);
					notifyAdmins(sender, String.format("Stats for %s were set to something different", username), new Object [] {} );
					PacketCommand.sendCommand(actualName, arg[0], rep, sat);
					return;
				}
			}
			
			sender.sendChatToPlayer(ChatMessageComponent.createFromText("The player '" + username + "' can not be found!"));
		} else {
			sender.sendChatToPlayer(ChatMessageComponent.createFromText("Unknown command!"));
		}
	}

	@Override
	public String getCommandUsage(ICommandSender icommandsender) {
		return "/thirst reset [username] 'Resets the players current statistics.'\n"
			 + "/thirst set <bar> <saturation> [username] 'Sets the players stats to the given values.'\n"
			 + "/thirst add <bar replenish> <saturation> [username] 'Adds the stats to that player'";	
	}
}
