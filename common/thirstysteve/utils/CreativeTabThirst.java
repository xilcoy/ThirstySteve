package thirstysteve.utils;

import thirstysteve.ThirstySteve;
import net.minecraft.creativetab.CreativeTabs;

public class CreativeTabThirst extends CreativeTabs {

	public CreativeTabThirst(String label) {
		super(label);
	}

	@Override
	public int getTabIconItemIndex() {
		return ThirstySteve.canteen.itemID;
	}
}
