package thirstysteve.utils;

import thirstysteve.ThirstySteve;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.ICraftingHandler;

public class CraftingHandler implements ICraftingHandler {

	public void onCrafting(EntityPlayer player, ItemStack item, IInventory craftMatrix) {
		for (int i = 0; i < 9; i++) {
			if(craftMatrix.getStackInSlot(i) != null) {
				if(craftMatrix.getStackInSlot(i).itemID == ThirstySteve.filter.itemID) {
					if(craftMatrix.getStackInSlot(i).getItemDamage() != 4) {
						ItemStack Filter = new ItemStack(ThirstySteve.filter);
						Filter.damageItem(craftMatrix.getStackInSlot(i).getItemDamage() + 1, player);
						player.inventory.addItemStackToInventory(Filter);
					} else {
						player.inventory.addItemStackToInventory(new ItemStack(ThirstySteve.dFilter, 1));
					}
				} 
			}
		}
	}

	public void onSmelting(EntityPlayer player, ItemStack item) {
		
	}

}
