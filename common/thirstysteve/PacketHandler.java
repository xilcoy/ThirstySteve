package thirstysteve;

import java.io.*;
import java.util.*;

import thirstysteve.packets.*;

import com.google.common.io.*;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.server.MinecraftServer;
import net.minecraft.src.*;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.*;
import cpw.mods.fml.common.network.*;

public class PacketHandler implements IPacketHandler {
	public static boolean isRemote = false;
	// 0 = none, 1 = integrated, 2 = dedicated
	public static int typeOfServer;
	
	@Override
	public void onPacketData(INetworkManager manager, Packet250CustomPayload packet, Player player) {
		ByteArrayDataInput dat = ByteStreams.newDataInput(packet.data);
		int id = dat.readInt();
		Object[] extradata = { (EntityPlayer)player };
		switch(FMLCommonHandler.instance().getEffectiveSide()) {
			case SERVER: {
				switch(id) {
			 		case 1: ThirstySteve.INSTANCE.savePacket.readServer(id, dat, extradata); break;
			 		case 5: ThirstySteve.INSTANCE.hurtPlayer.readServer(id, dat, extradata); break;
			 		case 6: ThirstySteve.INSTANCE.statsPacket.readServer(id, dat, extradata); break;
			 		case 9: ThirstySteve.INSTANCE.potionPacket.readServer(id, dat, extradata); break;
				}
				break;
			}
			case CLIENT: {
				switch(id) {
					case 1: ThirstySteve.INSTANCE.savePacket.readClient(id, dat, extradata); break;
					case 2: ThirstySteve.INSTANCE.soundPacket.readClient(id, dat, extradata); break;
					case 7: ThirstySteve.INSTANCE.commandPacket.readClient(id, dat, extradata); break;
					case 8: ThirstySteve.INSTANCE.configPacket.readClient(id, dat, extradata); break;
				}
				break;
			}
		}
	}
	
	public void readClient(int id, ByteArrayDataInput data, Object[] extradata) {}
	public void readServer(int id, ByteArrayDataInput data, Object[] extradata) {}
}