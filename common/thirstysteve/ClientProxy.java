package thirstysteve;

import java.util.Random;

import thirstysteve.*;
import thirstysteve.blocks.*;
import thirstysteve.gui.*;
import thirstysteve.utils.KeyHandler;
import thirstysteve.packets.PacketHandleSave;
import thirstysteve.packets.PacketSendStats;
import thirstysteve.utils.*;
import cpw.mods.fml.client.*;
import cpw.mods.fml.client.registry.*;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.registry.*;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.*;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.src.*;
import net.minecraftforge.client.*;
import net.minecraftforge.common.*;
import net.minecraftforge.event.*;
import net.minecraftforge.event.world.WorldEvent;

public class ClientProxy extends CommonProxy {
	public boolean loadedMod;
	private boolean changedGui;
	private int intDat;
	private boolean ranGame;
	private String sessionPlayer; // Can only be used once the player has been in a game.

	@Override
	public void onLoad() {
		TickRegistry.registerTickHandler(new TickHandler(), Side.CLIENT);
		TickRegistry.registerTickHandler(new TickHandler(), Side.SERVER);

		LanguageRegistry.addName(ThirstySteve.waterCollector, "Rain Collector");
		LanguageRegistry.addName(ThirstySteve.juiceMaker, "Drinks Brewer");
		LanguageRegistry.addName(ThirstySteve.underCollector, "Underground Water Collector");
		LanguageRegistry.addName(ThirstySteve.filter, "Clean Filter");
		LanguageRegistry.addName(ThirstySteve.dFilter, "Dirty Filter");
		LanguageRegistry.addName(ThirstySteve.ccFilter, "Charcoal Filter");
		LanguageRegistry.instance().addStringLocalization("itemGroup.thirstTab", "Thirsty Steve");

		KeyBindingRegistry.registerKeyBinding(new KeyHandler());
	}

	@Override
	public void onTickInGame() {
		Minecraft minecraft = FMLClientHandler.instance().getClient();
		if (minecraft.currentScreen != null) {
			onTickInGUI(minecraft.currentScreen);
		}

		if (minecraft.thePlayer != null) {
			PlayerHandler.addPlayer(getPlayer().username, new PlayerHandler(getPlayer(), new ThirstStats()));
			sessionPlayer = getPlayer().username;
			if (getPlayer().capabilities.isCreativeMode == false) {
				if (loadedMod == false) {
					if (changedGui == false) {
						minecraft.ingameGUI = new GuiThirstySteve();
						changedGui = true;
					}
					loadedMod = true;
				}
				
				if(ThirstUtils.isClientHost() == true) {
					String[] usernames = FMLCommonHandler.instance().getMinecraftServerInstance().getAllUsernames();
					for(int i = 0; i < usernames.length; i++) {
						if(PlayerHandler.getPlayer(usernames[i]) != null) {
							PlayerHandler.getPlayer(usernames[i]).getStats().onTick(getPlayer(), getPlayerMp(usernames[i]));
						}
					}
				} else PlayerHandler.getPlayer(sessionPlayer).getStats().onTick(getPlayer(), null);
				
				ranGame = true;
			}
			
			if(FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT) {
				addDrinkStats(minecraft.thePlayer);
			}

			intDat++;
			if (PacketHandler.isRemote == true & intDat > 20) {
				if(FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT) {
					PacketHandleSave.sendSaveData(getPlayer().username, PlayerHandler.getPlayer(getPlayer().username).getStats());
				}
			}
		}
	}

	public void onTickInGUI(GuiScreen gui) {
		if (gui instanceof GuiMainMenu) {
			PlayerHandler.ALL_PLAYERS.clear();
			loadedMod = false;
		}
		if (gui instanceof GuiGameOver) {
			PlayerHandler.getPlayer(sessionPlayer).setDefaults();
		}
	}
	
	@SideOnly(Side.CLIENT)
	public void addDrinkStats(EntityPlayer player) {
		if(player.getItemInUse() != null) {
			ItemStack item = player.getItemInUse();
			if(player.getItemInUseDuration() == item.getMaxItemUseDuration() - 1) {
				switch(item.itemID) {
					case 373: {
						PlayerHandler.getPlayer(player.username).getStats().addStats(5, 1.2f);
						PlayerHandler.getPlayer(player.username).getStats().getPoison().startPoison(new Random(), 0.3f);
						PacketSendStats.sendStatsToAdd(5, 1.2f, true, 0.3f);
						break;
					}
					case 282: {
						PlayerHandler.getPlayer(player.username).getStats().addStats(8, 4.2f);
						PacketSendStats.sendStatsToAdd(8, 4.2f, false, 0f);
						break;
					}
					case 335: {
						PlayerHandler.getPlayer(player.username).getStats().addStats(10, 5.1f);
						PacketSendStats.sendStatsToAdd(10, 5.1f, false, 0f);
						break;
					}
				}
				
				if(item.getItem() instanceof ItemFood) {
					PlayerHandler.getPlayer(player.username).getStats().addStats(-2, -3f);
					PacketSendStats.sendStatsToAdd(-2, -3f, false, 0f);
				}
			}
		} 
	}

	/**
	 * Gets an EntityPlayer.class instance.
	 * @return EntityPlayer.class instance.
	 */
	public static EntityPlayer getPlayer() {
		return FMLClientHandler.instance().getClient().thePlayer;
	}

	/**
	 * Gets an EntityPlayerMP.class instance. Only works client server.
	 * @return EntityPlayerMP.class instance.
	 */
	public static EntityPlayerMP getPlayerMp(String username) {
		MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();
		EntityPlayerMP player = server.getConfigurationManager().getPlayerForUsername(username);
		return player;
	}
}
