package thirstysteve;

import java.util.*;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import thirstysteve.api.*;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.potion.Potion;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.common.MinecraftForge;
import thirstysteve.packets.PacketHurtPlayer;
import thirstysteve.packets.PacketPotionEffect;
import thirstysteve.utils.*;

public class PoisonController {
	private int poisonTimer;
	private int healthPoison;
	private boolean poisonPlayer = false;
	private boolean preloadedPoison = false;
	private boolean isPoisoned = false;
	private boolean loadedClass = false;
	private int poisonID;
	public boolean drankPoisonStoper = false;
	private boolean addedPotion = false;
	
	/**
	 * PoisonControler ticks. Called from PlayerStatistics.class.
	 */
	public void onTick(EntityPlayer player) {
		if (shouldPoison() == true) {
			if(preloadedPoison == false) {
				Random random = new Random();
				poisonID = random.nextInt(4);
				preloadedPoison = true;
			}
			if(drankPoisonStoper == true) {
				poisonTimer = 0;
				isPoisoned = false;
				poisonPlayer = false;
			}
			addPoisonType(poisonID, player);
		}
	}

	/**
	 * Starts the poison.
	 */
	public void startPoison() {
		preloadedPoison = false;
		poisonPlayer = true;
		drankPoisonStoper = false;
		addedPotion = false;
	}
	
	private void addPoisonType(int i, EntityPlayer player) {
		if(i == 0) fastDehydration(player);
		else if(i == 1) addBadPotion(player);
		else if(i == 2) healthPoison(player);
	}
	
	public void fastDehydration(EntityPlayer player) {
		if(shouldPoison()) {
			MinecraftForge.EVENT_BUS.post(new ThirstAPI.OnPlayerPoisoned(360 - poisonTimer));
			poisonTimer++;
			PlayerHandler.getPlayer(player.username).addExhaustion(0.061111111111111f);
			isPoisoned = true;
			if (poisonTimer > 360) {
				poisonTimer = 0;
				isPoisoned = false;
				poisonPlayer = false;
			}
		}
	}
	
	public void addBadPotion(EntityPlayer player) {
		if(shouldPoison()) {
			MinecraftForge.EVENT_BUS.post(new ThirstAPI.OnPlayerPoisoned(680 - poisonTimer));
			poisonTimer++;
			isPoisoned = true;
			
			if (addedPotion == false) {
				if(FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT) {
					PacketPotionEffect.sendPacket(Potion.moveSlowdown.id, 18, 1);
					PacketPotionEffect.sendPacket(Potion.weakness.id, 18, 1);
				}
				addedPotion = true;
			}
			
			if (poisonTimer > 680) {
				poisonTimer = 0;
				isPoisoned = false;
				poisonPlayer = false;
			}
		}
	}
	
	public void healthPoison(EntityPlayer player) {
		if(shouldPoison()) {
			MinecraftForge.EVENT_BUS.post(new ThirstAPI.OnPlayerPoisoned(360 - poisonTimer));
			poisonTimer++;
			healthPoison++;
			isPoisoned = true;
			
			if(healthPoison > 40) {
				if(FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT) {
					PacketHurtPlayer.sendPacket();
				}
				healthPoison = 0;
			}
			
			if(addedPotion == false) {
				if(FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT) {
					PacketPotionEffect.sendPacket(Potion.confusion.id, 18, 1);
				}
				addedPotion = true;
			}
			
			if (poisonTimer > 360) {
				poisonTimer = 0;
				isPoisoned = false;
				poisonPlayer = false;
			}
		}
	}
	
	/**
	 * Poisons the player depending on chance
	 */
	public void startPoison(Random random, float f) {
		if(random.nextFloat() < f) {
			startPoison();
		}
	}

	/**
	 * Checks if the game should poison.
	 * @return if can poison.
	 */
	public boolean shouldPoison() {
		if (poisonPlayer == true && MinecraftForge.EVENT_BUS.post(new ThirstAPI.ShouldPoison()) == false) {
			return true;
		} 
		return false;
	}

	/**
	 * Checks if the player is currently poisoned.
	 * @return if the player is poisoned.
	 */
	public boolean isPoisoned() {
		return isPoisoned;
	}

	/**
	 * Checks how much time is remaining until the poison stops.
	 * @return remaining time until poison stops.
	 */
	public int poisonTimeRemain() {
		if (poisonTimer > 0) {
			return poisonTimer;
		} else {
			return 0;
		}
	}

	/**
	 * Shows the poison rate for a particular biome. If biome is not found will
	 * return 0.3f or "Other".
	 * @param biome String of biomes name. Available in BiomeGenBase.class.
	 * @return poison rate in float.
	 */
	public static float getBiomePoison(BiomeGenBase biome) {
		if (biome == BiomeGenBase.river || biome == BiomeGenBase.frozenRiver) {
			return 0.1f;
		} else if (biome == BiomeGenBase.ocean || biome == BiomeGenBase.frozenOcean) {
			return 0.9f;
		} else if (BiomeDictionary.isBiomeOfType(biome, BiomeDictionary.Type.DESERT)) {
			return 0.35f;
		} else if (BiomeDictionary.isBiomeOfType(biome, BiomeDictionary.Type.SWAMP)) {
			return 0.8f;
		} else if (BiomeDictionary.isBiomeOfType(biome, BiomeDictionary.Type.MUSHROOM)) {
			return 0.5f;
		}
		return 0.3f;
	}
	
	public void setPoisonedTo(boolean what) {
		poisonPlayer = what;
	}
	
	public void setPoisonTime(int what) {
		poisonTimer = what;
	}
}
