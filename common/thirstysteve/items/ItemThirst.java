package thirstysteve.items;

import java.util.Random;

import thirstysteve.ThirstySteve;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumMovingObjectType;
import net.minecraft.util.Icon;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeGenJungle;

public class ItemThirst extends Item {

	private ItemStack waterFillItem;
	private Item returnItem;
	
	public ItemThirst(int id) {
		super(id);
		waterFillItem = null;
		setCreativeTab(ThirstySteve.thirstTab);
		setNoRepair();
	}

	public ItemThirst setWaterFillItem(ItemStack item) {
		waterFillItem = item;
		return this;
	}

	public ItemStack getWaterFillItem() {
		return waterFillItem;
	}

	/**
	 * Sets the item that is returned.
	 * @param item Item that is returned after the drink is drunk.
	 * @return this.
	 */
	public ItemThirst setReturn(Item item) {
		returnItem = item;
		return this;
	}

	/**
	 * Gets the return item for this drink.
	 * @return the item that will be given after this drink is drunk.
	 */
	public Item getReturn(ItemStack itemStack) {
		return returnItem;
	}

	@Override
	public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer player) {
		if (waterFillItem == null) {
			return itemStack;
		}

		MovingObjectPosition mop = this.getMovingObjectPositionFromPlayer(world, player, true);
		
		if(mop == null || mop.typeOfHit != EnumMovingObjectType.TILE) {
			return itemStack;
		}

		int x = mop.blockX;
		int y = mop.blockY;
		int z = mop.blockZ;
		Random random = new Random();

		if(world.getBlockMaterial(x, y, z) == Material.water
		   || (world.getBlockId(x, y, z) == Block.leaves.blockID
			   && world.getBiomeGenForCoords(x, z) instanceof BiomeGenJungle
			   && random.nextFloat() < 0.3f)) {
		    if (itemStack.itemID == waterFillItem.itemID) {
		    	if (itemStack.getMaxDamage() > 0) {
		    		itemStack.setItemDamage(0);
		    	}
		    } else if (itemStack.stackSize == 1) {
			    itemStack = waterFillItem.copy();
		    } else if (player.inventory.addItemStackToInventory(waterFillItem.copy())) {
				itemStack.stackSize--;
		    	if (itemStack.getMaxDamage() > 0) {
		    		itemStack.setItemDamage(0);
		    	}
			}
		}

		return itemStack;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister iconRegister)
	{
	    this.itemIcon = iconRegister.registerIcon(String.format("thirstysteve:%s", getUnlocalizedName().substring(5)));
	}
}
