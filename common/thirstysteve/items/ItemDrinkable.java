/**
 * ThirstySteves Drink class. Similar to ItemFood.
 */
package thirstysteve.items;

import java.util.*;

import thirstysteve.utils.*;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.*;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.Icon;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import thirstysteve.*;

public class ItemDrinkable extends ItemThirst {
    /** Number of ticks to run while 'EnumAction'ing until result. */
    public final int itemUseDuration;
    
	private int thirstReplenish;
	private float saturationReplenish;
	private boolean alwaysDrinkable;
	private int potionId;
	private int potionDuration;
	private int potionAmplifier;
	private float potionEffectProbability;
	private boolean hasEffect = false;
	private Random rand = new Random();
	public int storeRecipe;
	public String inName;
	public String username;

	private float poisonChance;

	private int foodHeal;
	private float satHeal;

	public ItemDrinkable(int id, int replenish, float saturation, boolean alwaysDrinkable) {
		super(id);
		foodHeal = 0;
		satHeal = 0;
		itemUseDuration = 32;
		if (alwaysDrinkable == true) {
			this.alwaysDrinkable = true;
		}
		setThirstReplenish(replenish);
		setSaturationReplenish(saturation);
		setReturn(Item.glassBottle);
	}

	@Override
	public ItemStack onEaten(ItemStack itemstack, World world, EntityPlayer entityplayer) {		
		if (itemstack.getMaxDamage() > 0) {
			itemstack.setItemDamage(itemstack.getItemDamage() + 1);
		}

		PlayerHandler.getPlayer(entityplayer.username).getStats().addStats(getThirstReplenish(itemstack), getSaturationReplenish(itemstack));
		if (getPoisonChance(itemstack) > 0 && ConfigHelper.poisonOn == true) {
			Random rand = new Random();
			if(rand.nextFloat() < getPoisonChance(itemstack)) {
				PlayerHandler.getPlayer(entityplayer.username).getStats().getPoison().startPoison();
			}
		}

		if (getFoodHeal(itemstack) > 0 && getSatHeal(itemstack) > 0) {
			entityplayer.getFoodStats().addStats(getFoodHeal(itemstack), getSatHeal(itemstack));
		}

		if(getPotionID(itemstack) > 0 && world.rand.nextFloat() < getPotionEffectProbability(itemstack)) {
			entityplayer.addPotionEffect(new PotionEffect(getPotionID(itemstack), getPotionDuration(itemstack) * 20, getPotionAmplifier(itemstack)));
		}

		world.playSoundAtEntity(entityplayer, "random.burp", 0.5F, world.rand.nextFloat() * 0.1F + 0.9F);

		if (!world.isRemote && getPotionID(itemstack) > 0 && world.rand.nextFloat() < getPotionEffectProbability(itemstack)) {
			entityplayer.addPotionEffect(new PotionEffect(getPotionID(itemstack), getPotionDuration(itemstack) * 20, getPotionAmplifier(itemstack)));
		}

		if (itemstack.getMaxDamage() == 0 || itemstack.getItemDamage() >= itemstack.getMaxDamage()) {
			if (itemstack.stackSize == 1) {
				itemstack = new ItemStack(getReturn(itemstack));
			} else {
			    --itemstack.stackSize;
			    if (itemstack.getMaxDamage() > 0) {
				    itemstack.setItemDamage(0);
			    }
			    if (!entityplayer.inventory.addItemStackToInventory(new ItemStack(getReturn(itemstack)))) {
			    	entityplayer.dropPlayerItem(new ItemStack(getReturn(itemstack)));
			    }
			}
		}

		return itemstack;
	}

	public int getPotionAmplifier(ItemStack itemstack) {
		return potionAmplifier;
	}

	public int getPotionDuration(ItemStack itemstack) {
		return potionDuration;
	}

	public int getPotionID(ItemStack itemstack) {
		return potionId;
	}

	public float getPotionEffectProbability(ItemStack itemstack) {
		return potionEffectProbability;
	}

	public int getMaxItemUseDuration(ItemStack itemstack) {
		return 32;
	}

	public EnumAction getItemUseAction(ItemStack itemstack) {
		return EnumAction.drink;
	}

	@Override
	public ItemStack onItemRightClick(ItemStack itemstack, World world, EntityPlayer entityplayer) {
		username = entityplayer.username;
		if (canDrink() == true || alwaysDrinkable(itemstack) == true
			|| entityplayer.capabilities.isCreativeMode == true
			|| (entityplayer.canEat(false) && getFoodHeal(itemstack) > 0)) {
			entityplayer.setItemInUse(itemstack, getMaxItemUseDuration(itemstack));
			return itemstack;
		}

		return super.onItemRightClick(itemstack, world, entityplayer);
	}

	
	public boolean alwaysDrinkable(ItemStack itemstack) {
		return alwaysDrinkable;
	}

	/**
	 * Sets a potion effect when the drink is drunk.
	 * @param i Potion ID
	 * @param j Duration
	 * @param k Amplifier
	 * @param f Probability
	 * @return
	 */
	public ItemDrinkable setPotionEffect(int i, int j, int k, float f) {
		potionId = i;
		potionDuration = j;
		potionAmplifier = k;
		potionEffectProbability = f;
		return this;
	}

	@Override
	public boolean hasEffect(ItemStack itemstack) {
		if (hasEffect == true) {
			return true;
		} else {
			return false;
		}
	}


	/**
	 * Makes the item shiny like Golden Apple.
	 * @return this
	 */
	public ItemDrinkable setHasEffect() {
		hasEffect = true;
		return this;
	}

	/**
	 * Allows the drink to heal the food bar.
	 * @param level amount level.
	 * @param saturation amount satuation.
	 * @return
	 */
	public ItemDrinkable healFood(int level, float saturation) {
		setFoodHeal(level);
		setSatHeal(saturation);
		return this;
	}

	/**
	 * Can the person drink.
	 * @return
	 */
	public boolean canDrink() {
		return PlayerHandler.getPlayer(username).getStats().level < 20;
	}

	/**
	 * Sets a poisoning chance when drunk.
	 * @param chance Chance. 0.6f = 60% approximately.
	 * @return
	 */
	public float getPoisonChance(ItemStack itemstack) {
		return poisonChance;
	}

	public ItemDrinkable setPoisonChance(float poisonChance) {
		this.poisonChance = poisonChance;
		return this;
	}

	public int getThirstReplenish(ItemStack itemstack) {
		return thirstReplenish;
	}

	public ItemDrinkable setThirstReplenish(int thirstReplenish) {
		this.thirstReplenish = thirstReplenish;
		return this;
	}

	public float getSaturationReplenish(ItemStack itemstack) {
		return saturationReplenish;
	}

	public ItemDrinkable setSaturationReplenish(float saturationReplenish) {
		this.saturationReplenish = saturationReplenish;
		return this;
	}

	public int getFoodHeal(ItemStack itemstack) {
		return foodHeal;
	}

	public ItemDrinkable setFoodHeal(int foodHeal) {
		this.foodHeal = foodHeal;
		return this;
	}

	public float getSatHeal(ItemStack itemstack) {
		return satHeal;
	}

	public ItemDrinkable setSatHeal(float satHeal) {
		this.satHeal = satHeal;
		return this;
	}

	public ItemDrinkable setPotionEffectProbability(float potionEffectProbability) {
		this.potionEffectProbability = potionEffectProbability;
		return this;
	}
}
