package thirstysteve.items;

import java.util.List;
import java.util.Random;

import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.Icon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import thirstysteve.PlayerHandler;
import thirstysteve.ThirstySteve;
import thirstysteve.drink.Drink;
import thirstysteve.items.ItemDrinkable;
import thirstysteve.utils.ConfigHelper;

public class ItemDrink extends ItemDrinkable {
	private Icon [] icon = new Icon[32];
	
	public ItemDrink(int id) {
		super(id, 0, 0f, false);
		this.setHasSubtypes(true);
		this.setMaxDamage(0);
	}

	public Drink getDrink(ItemStack itemstack)
	{
		int i = MathHelper.clamp_int(itemstack.getItemDamage(), 0, 31);
		return Drink.drinkTypes[i];
	}
	
	/**
	 * Gets an icon index based on an item's damage value
	 */
	@Override
	public Icon getIconFromDamage(int damage)
	{
		int i = MathHelper.clamp_int(damage, 0, 31);
		return icon[i];
	}

    /**
     * Sets the unlocalized name of this item to the string passed as the parameter, prefixed by "item."
     */
	@Override
    public Item setUnlocalizedName(String str)
    {
		super.setUnlocalizedName("drink." + str);
        return this;
    }

	/**
	 * Returns the unlocalized name of this item. This version accepts an ItemStack so different stacks can have
	 * different names based on their damage or NBT.
	 */
	@Override
	public String getUnlocalizedName(ItemStack itemstack)
	{
		int i = MathHelper.clamp_int(itemstack.getItemDamage(), 0, 31);
		return getUnlocalizedName() + "." + Drink.drinkTypes[i].getName();
	}

	/**
	 * Callback for item usage. If the item does something special on right clicking, he will have one of those. Return
	 * True if something happen and false if it don't. This is for ITEMS, not BLOCKS
	 */
	@Override
	public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int par4, int par5, int par6, int par7, float par8, float par9, float par10)
	{
		return false;
	}

	@Override
	public int getPotionAmplifier(ItemStack itemstack) {
		return getDrink(itemstack).getPotionAmplifier();
	}

	@Override
	public int getPotionDuration(ItemStack itemstack) {
		return getDrink(itemstack).getPotionDuration();
	}

	@Override
	public int getPotionID(ItemStack itemstack) {
		return getDrink(itemstack).getPotionId();
	}

	@Override
	public float getPotionEffectProbability(ItemStack itemstack) {
		return getDrink(itemstack).getPotionEffectProbability();
	}

	@Override
	public boolean alwaysDrinkable(ItemStack itemstack) {
		return getDrink(itemstack).isAlwaysDrinkable();
	}

	@Override
	public boolean hasEffect(ItemStack itemstack) {
		return getDrink(itemstack).hasEffect();
	}
	
	@Override
	public int getThirstReplenish(ItemStack itemstack) {
		return getDrink(itemstack).getThirstReplenish();
	}
	
	@Override
	public float getSaturationReplenish(ItemStack itemstack) {
		return getDrink(itemstack).getThirstSaturation();
	}

	@Override
	public int getFoodHeal(ItemStack itemstack) {
		return getDrink(itemstack).getHungerReplenish();
	}

	@Override
	public float getSatHeal(ItemStack itemstack) {
		return getDrink(itemstack).getHungerSaturation();
	}

	/**
	 * Sets a poisoning chance when drunk.
	 * @param chance Chance. 0.6f = 60% approximately.
	 * @return
	 */
	@Override
	public float getPoisonChance(ItemStack itemstack) {
		return getDrink(itemstack).getPoisonChance();
	}

	@SideOnly(Side.CLIENT)
	@Override
	/**
	 * returns a list of items with the same ID, but different meta (eg: dye returns 16 items)
	 */
	public void getSubItems(int id, CreativeTabs tab, List itemList)
	{
		for (int i = 0; i < 32; ++i)
		{
			if (Drink.drinkTypes[i] != null) {
				if (this.itemID == ThirstySteve.bottle.itemID && Drink.drinkTypes[i].getName() == "water")
					continue;
				itemList.add(new ItemStack(id, 1, i));
			}
		}
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerIcons(IconRegister iconRegister)
	{
		for (int i = 0; i < 32; ++i)
		{
			if (Drink.drinkTypes[i] != null) {
				if (this.itemID == ThirstySteve.bottle.itemID && Drink.drinkTypes[i].getName() == "water")
					continue;
				this.icon[i] = iconRegister.registerIcon(String.format("thirstysteve:%s_%s", getUnlocalizedName().substring(11), Drink.drinkTypes[i].getName()));
			}
		}
	}
}
