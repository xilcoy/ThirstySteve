## ThirstySteve - Minecraft Forge

# About
This mod adds a thirst bar above your hunger bar that depletes half as fast
as your hunger. It does so when you undergo physical activity such as
running, hurting mobs and getting hurt yourself.

# Credits
Forked from ThirstMod by tarun1998. https://github.com/tarun1998/ThirstMod
Updated by Xilcoy
